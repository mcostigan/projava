package edu.uchicago.gerber._05dice.P10_19;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RestaurantDriver {
    public static void main(String[] args) {
        Bill myBill = new Bill();

        myBill.setSize(400,400);

        myBill.setDefaultCloseOperation(Bill.EXIT_ON_CLOSE);

        myBill.setVisible(true);
    }
}

