package edu.uchicago.gerber._05dice.P10_19;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Bill extends JFrame {
    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 250;

    private static final String[] ITEMS = {"Eggs", "Bacon", "Sausage", "Toast", "Fruit", "Orange Juice", "Milk", "Pancakes", "Waffles", "Ham"};
    private static final double[] PRICES = {1, 1.50, 2, 2.50, 3, 3, 2.50, 2, 1.50, 1};

    private JLabel billLabel;
    private JLabel taxLabel;
    private JLabel tipLabel;
    private JLabel totalLabel;
    private JTextField otherPrice;

    private double dBill;

    public Bill() {
        setLayout(new GridLayout(12, 1));

        for (int i = 0; i < 10; i++) {
            JPanel panel = new JPanel();
            JButton button = new JButton(String.valueOf(PRICES[i]));
            button.addActionListener(new ButtonClickListener(PRICES[i]));
            panel.add(new JLabel(ITEMS[i]));
            panel.add(button);
            add(panel);

        }
        dBill = 0;

        billLabel = new JLabel("Subtotal: 0.00");
        taxLabel = new JLabel("Tax: 0.00");
        tipLabel = new JLabel("Tip: 0.00");
        totalLabel = new JLabel("Total: 0.00");

        JPanel otherPanel = new JPanel();
        otherPanel.add(new JLabel("Other :"));

        otherPrice = new JTextField();
        otherPrice.setColumns(5);
        otherPanel.add(otherPrice);

        JButton button = new JButton("Add");
        button.addActionListener(new OtherItemListener());

        otherPanel.add(button);
        add(otherPanel);

        JPanel totalPanel = new JPanel();
        totalPanel.add(billLabel);
        totalPanel.add(taxLabel);
        totalPanel.add(tipLabel);
        totalPanel.add(totalLabel);
        add(totalPanel);

    }

    class ButtonClickListener implements ActionListener {
        private final double price;

        public ButtonClickListener(double price) {
            this.price = price;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            dBill += price;
            billLabel.setText("Subtotal: " + Math.round(dBill * 100.0) / 100.0);
            taxLabel.setText("Tax: " + Math.round(dBill*.1 * 100.0) / 100.0);
            tipLabel.setText("Subtotal: " + Math.round(dBill*.15 * 100.0) / 100.0);
            totalLabel.setText("Subtotal: " + Math.round(dBill*1.25 * 100.0) / 100.0);
            System.out.println(dBill);
        }
    }

    class OtherItemListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            dBill += Double.parseDouble(otherPrice.getText());
            billLabel.setText("Subtotal: " + Math.round(dBill * 100.0) / 100.0);
            taxLabel.setText("Tax: " + Math.round(dBill*.1 * 100.0) / 100.0);
            tipLabel.setText("Subtotal: " + Math.round(dBill*.15 * 100.0) / 100.0);
            totalLabel.setText("Subtotal: " + Math.round(dBill*1.25 * 100.0) / 100.0);
            otherPrice.setText("");
            System.out.println(dBill);

        }
    }

}

