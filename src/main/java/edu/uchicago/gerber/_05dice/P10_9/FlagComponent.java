package edu.uchicago.gerber._05dice.P10_9;

//import jdk.internal.org.jline.utils.Colors;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class FlagComponent extends JComponent {
    private final Color colorTop;
    private final Color colorMid;
    private final Color colorBottom;
    private final int xLeft;
    private final int yTop;
    private final int width;

    public FlagComponent(Color colorTop, Color colorMid, Color colorBottom, int xLeft, int yTop, int width) {
        super();
        this.colorTop = colorTop;
        this.colorMid = colorMid;
        this.colorBottom = colorBottom;
        this.xLeft = xLeft;
        this.yTop = yTop;
        this.width = width;
    }

    @Override
    protected void paintComponent(Graphics g) {
        drawFlag(g);
    }

    void drawFlag(Graphics g) {
        Random randColor = new Random();
        int HEIGHT = width * 2 / 3;
        int BAR_HEIGHT = HEIGHT / 3;

        g.setColor(colorTop);
        g.fillRect(xLeft, yTop, width, BAR_HEIGHT);
        g.setColor(colorMid);
        g.fillRect(xLeft, yTop + BAR_HEIGHT, width, BAR_HEIGHT);
        g.setColor(colorBottom);
        g.fillRect(xLeft, yTop + BAR_HEIGHT * 2, width, BAR_HEIGHT);

        g.setColor(Color.BLACK);
        g.drawLine(xLeft, yTop, xLeft + width, yTop);
        g.drawLine(xLeft, yTop + HEIGHT, xLeft + width, yTop + HEIGHT);
        g.drawLine(xLeft, yTop , xLeft, yTop + HEIGHT);
        g.drawLine(xLeft+width, yTop , xLeft+width, yTop + HEIGHT);
    }

}
