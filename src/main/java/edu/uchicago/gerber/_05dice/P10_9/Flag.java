package edu.uchicago.gerber._05dice.P10_9;
import javax.swing.*;
import java.awt.*;

public class Flag {
    public static void main(String[] args) {

        JFrame frame = new JFrame();
        JComponent german = new FlagComponent(Color.BLACK, Color.RED, Color.YELLOW,10,100,180);
        JComponent hungarian = new FlagComponent(Color.RED, Color.WHITE, Color.GREEN,10,100,180);
        frame.setLayout(new GridLayout(1,2));
        frame.setSize(400, 400);

        frame.add(german,BorderLayout.NORTH);
        frame.add(hungarian,BorderLayout.SOUTH);


        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}


