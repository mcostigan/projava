package edu.uchicago.gerber._05dice.P10_10;


import javax.swing.*;
import java.awt.*;

public class Rings extends JComponent {

    @Override
    protected void paintComponent(Graphics g) {
        drawRing(g,0,0,100,Color.BLUE);
        drawRing(g,100,0,100,Color.BLACK);
        drawRing(g,200,0,100,Color.RED);
        drawRing(g,50,50,100,Color.YELLOW);
        drawRing(g,150,50,100,Color.GREEN);
    }

    public void drawRing(Graphics g, int x, int y, int diameter, Color ringColor) {
        g.setColor(ringColor);
        g.drawOval(x, y, diameter, diameter);
    }

}
