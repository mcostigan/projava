package edu.uchicago.gerber._05dice.P10_10;

import javax.swing.*;

public class OlympicDriver {
    public static void main(String[] args) {
        final int FRAME_WIDTH = 600;
        final int FRAME_HEIGHT = 300;

        JFrame frame = new JFrame();
        JPanel panel = new JPanel();

        Rings olympicRing = new Rings();
        frame.setSize(300, 150);

        frame.add(olympicRing);


        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
