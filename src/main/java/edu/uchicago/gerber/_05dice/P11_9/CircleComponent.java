package edu.uchicago.gerber._05dice.P11_9;

import javax.swing.*;
import java.awt.*;

public class CircleComponent extends JComponent {
    private int xCenter;
    private int yCenter;
    private int radius;

    public CircleComponent(int xCenter, int yCenter, int radius) {
        System.out.println(".");
        this.xCenter = xCenter;
        this.yCenter = yCenter;
        this.radius = radius;
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(Color.BLACK);
        g.drawOval(xCenter,yCenter,radius,radius);
    }

}
