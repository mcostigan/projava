package edu.uchicago.gerber._05dice.P11_9;



import javax.swing.*;

public class CircleViewer {
    public static void main(String[] args) {
        Circle myCircle = new Circle();
        myCircle.setDefaultCloseOperation(Circle.EXIT_ON_CLOSE);
        myCircle.setSize(400,400);
        myCircle.setVisible(true);

    }
}
