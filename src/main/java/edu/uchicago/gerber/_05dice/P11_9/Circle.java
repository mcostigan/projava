package edu.uchicago.gerber._05dice.P11_9;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Circle extends JFrame {
    private Point center;
    private int xCenter;
    private int yCenter;

    private Point edge;
    private int xEdge;
    private int yEdge;
    private int radius;
    private MouseListener listener;

    public Circle() throws HeadlessException {
        listener = new MyMouseListener();
        addMouseListener(listener);

    }


    class MyMouseListener implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {

        }

        @Override
        public void mousePressed(MouseEvent e) {
            if (center == null){
                System.out.println("?");
                center = e.getPoint();
                xCenter = center.x;
                yCenter = center.y;
            } else{
                System.out.println("!");
                edge=e.getPoint();
                xEdge=edge.x;
                yEdge=edge.y;
                radius = (int) Math.sqrt(Math.pow(xEdge-xCenter,2) + Math.pow(yEdge-yCenter,2));
                System.out.println(xCenter+" "+yCenter+" "+radius);

                add(new CircleComponent(xCenter-radius,yCenter-radius,radius*2));
                // my frame wasn't updating, this was suggested in an online source
                setVisible(false);
                setVisible(true);
                //reset
                center=null;
            }


        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }


    }
}
