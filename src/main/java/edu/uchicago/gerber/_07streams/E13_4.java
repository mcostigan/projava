package edu.uchicago.gerber._07streams;

public class E13_4 {
    public static void main(String[] args) {
        System.out.println(binary(6));

    }

    /**
     * @param i int to represent in binary
     * @return binary representation of i
     */
    public static String binary(int i) {
        // if i is 1, return 1
        if (i == 1) {
            return "1";
        }

        // else, if i is even, add 0 and find the binary rep of i/2
        if (i % 2 == 0) {
            return binary(i / 2) + "0";
        } else {
            // else add 1 and find the binary rep of i-1/2
            return binary((i - 1) / 2) + "1";
        }
    }
}

