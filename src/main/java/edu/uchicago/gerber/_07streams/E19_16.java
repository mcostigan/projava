package edu.uchicago.gerber._07streams;

import java.io.File;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class E19_16 {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("Mary", "had","a","little","lamb");
        Map< Object,Double> stream2 = stream.collect(Collectors.groupingBy(w-> w.charAt(0),Collectors.averagingInt(String::length)));
        System.out.println(stream2.toString());

    }
}
