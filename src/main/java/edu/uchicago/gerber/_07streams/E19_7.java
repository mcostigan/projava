package edu.uchicago.gerber._07streams;

import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class E19_7 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter words: ");
        String[] input = in.nextLine().split(" ");
        Stream<String> stream = Stream.of(input)
                .filter(w-> w.length()>1)
                .map(w-> w.charAt(0) + "..." + w.charAt(w.length()-1));

        System.out.println(stream.collect(Collectors.joining("\n")));


    }
}
