package edu.uchicago.gerber._07streams;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class P13_3 {
    public static void main(String[] args) {

        // get all possible phone number strings
        String[] result = telephoneWords(2633465282L);
        Set<String> words = new HashSet<String>();

        // create a hash set of all words
        try {
            words = readAllWords();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // print the first 100, because otherwise, there would be too many...
        int i=0;
        while(i<100) {
            ArrayList<String> tmp = wordBreak(result[i], words);
            for (String s1 : tmp) {
                System.out.println(s1);
                i++;
            }
        }
    }

    /**
     * Take a phone number and find all possible strings
     *
     * @param phoneNumber a phoneNumber represented as a long int
     * @return An array of strings that could be spelled with the given number
     */
    public static String[] telephoneWords(long phoneNumber) {
        // list of lists of all letters assigned to each number
        String[][] mappings = {{""}, {""}, {"A", "B", "C"}, {"D", "E", "F"}, {"G", "H", "I"}, {"J", "K", "L"}, {"M", "N", "O"}, {"P", "Q", "R", "S"}, {"T", "U", "V"}, {"W", "X", "Y", "Z"}};

        // if phone number is single digit, return the list of possible chars
        if (phoneNumber < 10) {
            return mappings[(int) phoneNumber];
        }

        // else, chop of last digit
        int digit = (int) (phoneNumber % 10);

        // make a recursive call to get possible strings of first n-1 digits
        String[] recursiveWords = telephoneWords(phoneNumber / 10);
        // initialize result array
        String[] result = new String[recursiveWords.length * mappings[digit].length];

        // for each char assigned to the lass digit, concatenate it with each result from the recursive calls
        int i = 0;
        for (String s : mappings[digit]) {
            for (String recursiveWord : recursiveWords) {
                result[i] = recursiveWord + s;
                i++;
            }
        }

        return result;

    }

    /**
     * @param s    String to break into words
     * @param dict Set of recognized words
     * @return an arraylist of all ways to break the string into words
     */
    public static ArrayList<String> wordBreak(String s, Set<String> dict) {
        // Initialize return data structure
        ArrayList<String> result = new ArrayList<String>();

        // If s is empty, return array list with empty string
        if (s.length() == 0) {
            result.add("");
            return result;
        }

        // Iterate through possible ending positions of the first word
        // If first word is in set, then recursively call on the rest of the string
        for (int i = 0; i <= s.length(); i++) {
            if (dict.contains(s.substring(0, i))) {
                ArrayList<String> recursiveBreak = wordBreak(s.substring(i), dict);
                for (String s1 : recursiveBreak) {
                    result.add(s.substring(0, i) + " " + s1);

                }
            }

        }

        return result;
    }

    /**
     * @return a hash set of all words listed in words.txt
     * @throws FileNotFoundException if file has been moved or deleted
     */
    public static Set<String> readAllWords() throws FileNotFoundException {
        //  Open word file initialize scanner and hash table
        try {
            File wordsFile = new File("/Users/matthewcostigan/projava/src/main/java/edu/uchicago/gerber/_07streams/words.txt");
            Scanner words = new Scanner(wordsFile);
            Set<String> result = new HashSet<String>();

            // loop through all words and add to hash set
            while (words.hasNext()) {
                result.add(words.next().toUpperCase(Locale.ROOT));
            }

            return result;
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("Invalid file path:/Users/matthewcostigan/projava/src/main/java/edu/uchicago/gerber/_07streams/words.txt. Has the file been moved or deleted?");
        }

    }
}
