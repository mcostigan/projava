package edu.uchicago.gerber._07streams;

import java.util.Currency;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class E19_6 {
    public static void main(String[] args) {
        Stream<Currency> stream =  Currency.getAvailableCurrencies().stream();
        Stream<String> displayNames = stream.map(Currency::getDisplayName)
                .sorted();

        System.out.println(displayNames.collect(Collectors.joining("\n")));




    }

}
