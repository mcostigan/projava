package edu.uchicago.gerber._07streams;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class E19_14 {
    public static void main(String[] args) {
        // open the book
        File fileInput = new File("/Users/matthewcostigan/projava/src/main/java/edu/uchicago/gerber/_07streams/warandpeace.txt");
        Scanner in;
        try {
            in = new Scanner(fileInput);
        } catch (FileNotFoundException e) {
            System.out.println("Invalid file path");
            return;
        }

        // covert book to array list
        ArrayList<String> fileWords = new ArrayList<String>();
        while (in.hasNext()){
            fileWords.add(in.next());
        }

        // extract a string from the stream: any palindrome of length 5+
        String s = fileWords.stream()
                .parallel()
                .filter(w-> w.length()>=5)
                .filter(E19_14::isPalindrome)
                .findAny()
                .orElse("");

        System.out.println(s);

    }

    /**
     *
     * @param s to check if palindrome
     * @return T/F indicator
     */
    public static boolean isPalindrome(String s){
        if (s.length()<=1){
            return true;
        }

        if (s.charAt(0)==s.charAt(s.length()-1)) {
            return isPalindrome(s.substring(1,s.length()-1));
        } else{
            return false;
        }

    }
}
