package edu.uchicago.gerber._07streams;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class E19_5 {
    public static void main(String[] args) {
        System.out.println(toString(Stream.of("Mary", "had","a","little","lamb"),4));


    }

    public static String toString(Stream<String> stream, int n){
        stream = stream
                .limit(n);
        return stream.collect(Collectors.joining(", "));
    }
}
