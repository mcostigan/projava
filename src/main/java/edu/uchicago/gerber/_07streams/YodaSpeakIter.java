package edu.uchicago.gerber._07streams;

public class YodaSpeakIter {
    public static void main(String[] args) {
        System.out.println(yodaSpeak("the force is strong with you"));
    }

    public static String yodaSpeak(String sentence) {
        String[] arrSentence = sentence.split(" ");
        StringBuilder result = new StringBuilder(arrSentence[arrSentence.length - 1]);

        // iterate backwards and append result
        for (int i = arrSentence.length - 2; i > -1; i--) {
            result.append(" ").append(arrSentence[i]);

        }
        return result.toString();

    }
}
