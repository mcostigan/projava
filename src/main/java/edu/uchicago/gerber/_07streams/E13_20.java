package edu.uchicago.gerber._07streams;

import java.util.ArrayList;
import java.util.Arrays;

public class E13_20 {
    public static void main(String[] args) {
        ArrayList<ArrayList<Integer>> result = waysToPay(127, new int[]{1, 5, 20, 100});
        for (ArrayList<Integer> integers : result) {
            System.out.println(integers);
        }

    }

    /**
     * Join bill less than or equal to the amount to the result of a recursive call
     *
     * @param amount total amount to make
     * @param bills  bills you can use to make amounts
     * @return An array list of array lists, where the inner level represents different ways to pay
     */
    public static ArrayList<ArrayList<Integer>> waysToPay(int amount, int[] bills) {
        // initialize return variables
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer> tmp = new ArrayList<Integer>();

        // return empty array if amount is negative
        if (amount < 0) {
            return result;
        }

        // if amount is less than 5, can only be made with 1s
        if (amount < 5) {
            for (int i = 0; i < amount; i++) {

                tmp.add(1);
            }
            result.add(tmp);
            return result;
        }

        // else, for each bill, check if <= to amount
        for (int i = 0; i < bills.length; i++) {
            if (amount >= bills[i]) {
                // if yes, make recursive call to find the amount less the bill, using only bills of equal or lesser value
                // to avoid duplicates, only use bills of equal or lesser value
                ArrayList<ArrayList<Integer>> recursivePay = waysToPay(amount - bills[i], Arrays.copyOfRange(bills, 0, i + 1));

                // for each array list in the result, append the current bill, and add the new list to the return list
                for (ArrayList<Integer> integers : recursivePay) {
                    integers.add(bills[i]);
                    result.add(integers);
                }
            }
        }
        return result;
    }
}
