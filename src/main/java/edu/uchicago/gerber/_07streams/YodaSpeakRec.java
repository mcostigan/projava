package edu.uchicago.gerber._07streams;

public class YodaSpeakRec {
    public static void main(String[] args) {
        System.out.println(yodaSpeak("the force is strong with you"));
    }

    public static String yodaSpeak(String sentence){
        if (!sentence.contains(" ")){
            return sentence;
        }

        // Pop last word
        int lastSpace = sentence.lastIndexOf(" ");
        String lastWord = sentence.substring(lastSpace+1);


        return lastWord + " " + yodaSpeak(sentence.substring(0,lastSpace));

    }
}
