package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P4_1A {
    public static void main(String[] args) {
        //Initialize scanner
        Scanner in = new Scanner(System.in);

        //a: the sum of all even numbers between 2 and 100
        //Initialize sum
        int iSum = 0;

        //Loop through all even ints (2,4,...100)
        for (int i = 1; i < 51; i++) {
            //Add to iSum
            iSum += i * 2;
        }

        //Output
        System.out.println(iSum);
    }
}
