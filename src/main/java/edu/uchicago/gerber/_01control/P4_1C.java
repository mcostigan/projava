package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P4_1C {
    public static void main(String[] args) {
        //Initialize scanner
        Scanner in = new Scanner(System.in);

        //c:all powers of 2 from 2^0 to 2^20
        //iterate through i = 1,2,3...,20
        for (int i = 0; i < 21; i++) {
            //output
            System.out.print("2^" + i + "=");
            System.out.println((int) Math.pow(2, i));
        }
        System.out.println();

    }
}
