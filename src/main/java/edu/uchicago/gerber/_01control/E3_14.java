package edu.uchicago.gerber._01control;

import java.util.Scanner;


public class E3_14 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int iMonth, iDay;
        String sSeason;

        //Prompt for month
        System.out.println("Enter month: ");

        //Prompt until valid input
        while (!(in.hasNextInt())) {
            System.out.println("Oops! Please enter an integer:");
            in.next();
        }

        //Set month variable
        iMonth = in.nextInt();

        //Prompt for day
        System.out.println("Enter day: ");

        //Prompt until valid input
        while (!(in.hasNextInt())) {
            System.out.println("Oops! Please enter an integer:");
            in.next();
        }

        //Set day variable
        iDay = in.nextInt();

        //If month is 1, 2, or 3, season = "Winter"
        if (iMonth < 4) {
            sSeason = "Winter";
        }
        //Else if month is 4, 5, or 6, season = "Spring"
        else if (iMonth < 7) {
            sSeason = "Spring";
        }
        //Else if month is 7, 8, or 9, season = "Summer"
        else if (iMonth < 10) {
            sSeason = "Summer";
        }
        //Else if month is 10, 11, or 12, season = "Fall"
        else {
            sSeason = "Fall";
        }
        //If month is divisible by 3 and day >= 21
        if (iMonth % 3 == 0 && iDay >= 21) {
            switch (sSeason) {
                //If season is "Winter", season = "Spring"
                case "Winter":
                    sSeason = "Spring";
                    break;

                //Else if season is "Spring", season = "Summer"
                case "Spring":
                    sSeason = "Summer";
                    break;

                //Else if season is "Summer", season = "Fall"
                case "Summer":
                    sSeason = "Fall";
                    break;

                //Else season = "Winter"
                default:
                    sSeason = "Winter";
                    break;
            }
        }
        System.out.println(sSeason);
    }
}

