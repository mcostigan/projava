package edu.uchicago.gerber._01control;

import java.util.Scanner;


public class P4_17 {
    public static void main(String[] args) {
        //Initialize scanner
        Scanner in = new Scanner(System.in);

        //Prompt for integer
        System.out.println("Enter an integer:");

        //Prompt until valid input
        while (!(in.hasNextInt())) {
            System.out.println("Oops! Please enter an integer:");
            in.next();
        }
        int n = in.nextInt();

        //Initialize binary string, which will read start with 2^0 on the far left
        //Used string builder at suggestion of IntelliJ
        StringBuilder sBin = new StringBuilder();


        do {
            //Add remained when n is divided by 2 to binary string
            sBin.append(n % 2);
            //Divide n by two using integer division
            n /= 2;
        }
        //Continue until n==0
        while (n > 0);

        //Print sBin in reverse, so the binary string reads right to left
        for (int i = sBin.length() - 1; i >= 0; i--) {
            System.out.println(sBin.charAt(i));

        }
    }
}

