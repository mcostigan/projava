package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P4_1E {
    public static void main(String[] args) {
        //Initialize scanner
        Scanner in = new Scanner(System.in);

        //Initialize sum
        int iSum = 0;

        //e) the sum of all odd digits of an input
        //Prompt for integer
        System.out.println("Enter an integer:");

        //Prompt until valid input
        while (!(in.hasNextInt())) {
            System.out.println("Oops! Please enter an integer:");
            in.next();
        }

        //Set input variable
        int iInput = in.nextInt();

        //complete steps while iInput > 0
        do {
            //If iInput is odd, then last digit is odd
            if (iInput % 2 != 0) {
                //Get value of last digit by finding remainder when divided by 10
                //And add to iSum
                iSum += iInput % 10;
            }
            //Remove last digit using integer division
            iInput /= 10;
        }
        //If iInput is 0, exit. else, do again
        while (iInput > 0);

        //output
        System.out.println(iSum);
    }
}
