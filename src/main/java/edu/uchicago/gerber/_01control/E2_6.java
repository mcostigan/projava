package edu.uchicago.gerber._01control;

import java.util.Scanner;


public class E2_6 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        //Measurement constants
        final double MILES_PER_METER = 0.000621371;
        final double FEET_PER_METER = 3.28084;
        final double INCHES_PER_MILE = 39.3701;

        //Initialize distance as double
        double dMeters;

        //Prompt for distance
        System.out.println("Please enter a distance in meters (any non-negative, numeric value): ");

        //Prompt until valid
        while (!in.hasNextDouble()) {
            System.out.println("Oops! Please enter an valid distance:");
            in.next();
        }

        //Store value
        dMeters = in.nextDouble();


        //Distance in miles
        System.out.print("Miles:");
        System.out.println(dMeters * MILES_PER_METER);

        //Distance in feet
        System.out.print("Feet:");
        System.out.println(dMeters * FEET_PER_METER);

        //Distance in inches
        System.out.print("Inches:");
        System.out.println(dMeters * INCHES_PER_MILE);
    }
}

