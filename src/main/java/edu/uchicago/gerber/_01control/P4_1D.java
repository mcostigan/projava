package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P4_1D {
    public static void main(String[] args) {
        //Initialize scanner
        Scanner in = new Scanner(System.in);

        //Initialize sum
        int iSum = 0;

        //d: the sum of all odd numbers between a and b inclusive

        //Prompt for integer
        System.out.println("Enter an integer:");

        //Prompt until valid input
        while (!(in.hasNextInt())) {
            System.out.println("Oops! Please enter an integer:");
            in.next();
        }

        //Set intOne variable
        int iA = in.nextInt();

        //Prompt for integer
        System.out.println("Enter an integer:");

        //Prompt until valid input
        while (!(in.hasNextInt())) {
            System.out.println("Oops! Please enter an integer:");
            in.next();
        }

        //Set intOne variable
        int iB = in.nextInt();

        int iStart = Math.min(iA, iB);
        int iEnd = Math.max(iA, iB);

        for (int i = iStart; i <= iEnd; i++) {
            //if i is odd, then it will have a remainder
            if (i % 2 != 0) {
                iSum += i;
            }
        }
        System.out.println(iSum);

    }
}
