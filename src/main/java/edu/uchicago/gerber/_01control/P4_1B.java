package edu.uchicago.gerber._01control;

import java.util.Scanner;

public class P4_1B {
    public static void main(String[] args) {
        //Initialize scanner
        Scanner in = new Scanner(System.in);

        //Initialize sum
        int iSum = 0;

        //b:the sum of all squares between 1 and 100

        int nIter = 1;
        while (nIter * nIter <= 100) {
            iSum += nIter * nIter;
            nIter++;
        }
        System.out.println(iSum);

    }
}
