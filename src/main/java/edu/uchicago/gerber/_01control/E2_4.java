package edu.uchicago.gerber._01control;

import java.util.Scanner;


public class E2_4 {
    public static void main(String[] args) {
        //Initialize scanner
        Scanner in = new Scanner(System.in);

        //Prompt for integer
        System.out.println("Enter an integer:");

        //Prompt until valid input
        while (!(in.hasNextInt())) {
            System.out.println("Oops! Please enter an integer:");
            in.next();
        }

        //Set iOne variable
        int iOne = in.nextInt();

        //Prompt for integer
        System.out.println("Enter another integer:");

        //Prompt until valid input
        while (!(in.hasNextInt())) {
            System.out.println("Oops! Please enter an integer:");
            in.next();
        }

        //Set iTwo variable
        int iTwo = in.nextInt();

        //Sum
        System.out.print("Sum: ");
        System.out.println(iOne + iTwo);

        //Difference
        System.out.print("Difference: ");
        System.out.println(iOne - iTwo);

        //Product
        System.out.print("Product: ");
        System.out.println(iOne * iTwo);

        //Average
        System.out.print("Average: ");
        System.out.println((iOne + iTwo) / 2.0);

        //Distance
        System.out.print("Distance: ");
        System.out.println(Math.abs(iOne - iTwo));

        //Max
        System.out.print("Max: ");
        System.out.println(Math.max(iOne, iTwo));

        //Min
        System.out.print("Min: ");
        System.out.println(Math.min(iOne, iTwo));
    }
}

