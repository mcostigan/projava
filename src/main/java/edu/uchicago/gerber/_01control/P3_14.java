package edu.uchicago.gerber._01control;

import java.util.Scanner;


public class P3_14 {
    public static void main(String[] args) {
        //Initialize scanner
        Scanner in = new Scanner(System.in);

        //Initialize variables
        int iYear;
        boolean bDivisibleBy4;
        boolean bDivisibleBy100;
        boolean bDivisibleBy400;


        //Prompt for integer
        System.out.println("Enter a year:");

        //Prompt until valid input
        while (!(in.hasNextInt())) {
            System.out.println("Oops! Please enter an integer:");
            in.next();
        }
        //Set iYear variable
        iYear = in.nextInt();

        //Check if year is divisible by 4, 100 and 400
        bDivisibleBy4 = iYear % 4 == 0;
        bDivisibleBy100 = iYear % 100 == 0;
        bDivisibleBy400 = iYear % 400 == 0;

        //If year is divisible by 400 OR (is divisibly by 4 AND not divisible by 100)
        if (bDivisibleBy400 || (bDivisibleBy4 && !bDivisibleBy100)) {
            //Then, leap year
            System.out.println("Yup, that's a leap year");
        } else {
            //Not a leap year
            System.out.println("No, that's not a leap year, silly!");
        }

    }
}

