package edu.uchicago.gerber._01control;

import java.util.Scanner;


public class P3_13 {
    public static void main(String[] args) {
        //Initialize scanner
        Scanner in = new Scanner(System.in);

        int iArabic;

        //Prompt for integer
        System.out.println("Enter an integer:");

        //Prompt until valid input
        while (!(in.hasNextInt())) {
            System.out.println("Oops! Please enter an integer:");
            in.next();
        }
        iArabic = in.nextInt();


        //String of Roman Numerals in ascending order
        String sNumerals = "IVXLCDM";

        //Used StringBuilder at suggestion of IntelliJ
        StringBuilder sRoman = new StringBuilder();

        //Single char strings to be defined for the ones, tens, and hundreds places
        String sSmall;
        String sMed;
        String sBig;

        //Create ones, tens, and hundreds places with a loop
        for (int i = 0; i < 3; i++) {
            //Each place has a small, med, and big numeral used to express value
            //ones: I,V,X, respectively
            //tens: X,L,C
            //hundreds: C,D,M
            //each place starts its numerals two characters ahead of the last
            sSmall = sNumerals.substring(i * 2, i * 2 + 1);
            sMed = sNumerals.substring(i * 2 + 1, i * 2 + 2);
            sBig = sNumerals.substring(i * 2 + 2, i * 2 + 3);

            //Assign numeral based on value of last digit
            switch (iArabic % 10) {
                case 0:
                    break;
                case 1:
                    sRoman.insert(0, sSmall);
                    break;
                case 2:
                    sRoman.insert(0, sSmall + sSmall);
                    break;
                case 3:
                    sRoman.insert(0, sSmall + sSmall + sSmall);
                    break;
                case 4:
                    sRoman.insert(0, sSmall + sMed);
                    break;
                case 5:
                    sRoman.insert(0, sMed);
                    break;
                case 6:
                    sRoman.insert(0, sMed + sSmall);
                    break;
                case 7:
                    sRoman.insert(0, sMed + sSmall + sSmall);
                    break;
                case 8:
                    sRoman.insert(0, sMed + sSmall + sSmall + sSmall);
                    break;
                case 9:
                    sRoman.insert(0, sSmall + sBig);
                    break;
            }

            //Truncate last digit with int division and repeat
            iArabic /= 10;
        }


        //Create thousands place with separate loop
        //Because of program specifications, iArabic can be 0,1,2, or 3 at the end of the previous loop
        //Add the number of Ms that corresponds to iArabic's value
        for (int i = 0; i < iArabic; i++) {
            sRoman.insert(0, "M");
        }

        //output
        System.out.println(sRoman);

    }


}

