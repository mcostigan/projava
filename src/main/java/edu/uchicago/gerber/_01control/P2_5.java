package edu.uchicago.gerber._01control;


import java.util.Scanner;

public class P2_5 {
    public static void main(String[] args) {
        double dPrice;
        int iDollars;
        double dDifference;
        int iCents;
        Scanner in = new Scanner(System.in);

        //Prompt for distance
        System.out.println("Please enter a price (any non-negative, numeric value): ");

        //Prompt until valid
        while (!in.hasNextDouble()) {
            System.out.println("Oops! Please enter an valid price:");
            in.next();
        }

        //Store value
        dPrice = in.nextDouble();


        //Assign price to an integer variable dollars
        iDollars = (int) dPrice;

        //Multiply the difference (price-dollars) by 100 and add .5
        dDifference = (dPrice - iDollars) * 100 + .5;

        //Assign the result to an integer variable cents
        iCents = (int) dDifference;

        //Print results
        System.out.print("Dollars: ");
        System.out.println(iDollars);
        System.out.print("Cents: ");
        System.out.println(iCents);
    }
}

