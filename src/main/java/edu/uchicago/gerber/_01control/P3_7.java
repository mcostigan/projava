package edu.uchicago.gerber._01control;

import java.util.Scanner;


public class P3_7 {
    public static void main(String[] args) {
        //Initialize scanner
        Scanner in = new Scanner(System.in);

        //Income bounds for tax rates, per P3.7 (ie 0-50,000, 50,000-75,000, etc.) These six values create 5 tax brackets, and everything exceeding 500,000 is taxed in its own bracket
        double[] dBrackets = {0, 50_000, 75_000, 100_000, 250_000, 500_000};
        //Tax rates, per 3.7
        //*Note: These 6 rates correspond to the 5 brackets defined in dBrackets[] and the final bracket that taxes all income over 500,000
        double[] dRates = {.01, .02, .03, .04, .05, .06};

        //Initialize variables
        double dIncome;
        double dIncrementalIncome;
        double dTax = 0;

        //Prompt for integer
        System.out.println("Enter your income:");

        //Prompt until valid input
        while (!(in.hasNextDouble())) {
            System.out.println("Oops! Please enter a valid income:");
            in.next();
        }

        //Save income to variable
        dIncome = in.nextDouble();

        //Loop through brackets/rates until all income has been taxed or last bracket is reached
        int nCounter = 1;
        while (dIncome > 0 && nCounter < dBrackets.length) {
            //Calculate amount of income in bracket
            dIncrementalIncome = Math.min(dBrackets[nCounter] - dBrackets[nCounter - 1], dIncome);
            //Add tax on incremental income to tax
            dTax += dIncrementalIncome * dRates[nCounter - 1];
            //Subtract incremental income from income
            dIncome -= dIncrementalIncome;
            //Increment counter
            nCounter++;
        }

        //Tax on all income above last tax bracket
        //Use max to account for possible negative incomes, where the liability should be 0
        dTax += Math.max(dIncome, 0) * dRates[dRates.length - 1];

        //Print tax liability
        System.out.print("Your tax liability is $");
        System.out.format("%.2f", dTax);

    }
}

