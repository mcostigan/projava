package edu.uchicago.gerber._08final.mvc.model;


import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collectors;


public class Debris extends Sprite {

    //overloaded so we can spawn smaller asteroids from an exploding one
    public Debris(Movable astExploded) {

        //calls sprite constructor
        super();

        //set team
        setTeam(Team.DEBRIS);

        // set random small size. Debris will shrink by one unit every time grame until it expires
        setRadius(Game.R.nextInt(10));
        setExpiry(getRadius());

        //set position and speed variables
        setCenter(astExploded.getCenter());
        setDeltaX(somePosNegValue(10));
        setDeltaY(somePosNegValue(10));
        setSpin(6);

        assignRandomShape();

    }


    @Override
    public void move() {
        super.move();
        //an asteroid spins, so you need to adjust the orientation at each move()
        setOrientation(getOrientation() + getSpin());
        setRadius(getRadius()-1);
        expire();

    }


    public void assignRandomShape() {

        //6.283 is the max radians
        final int MAX_RADIANS_X1000 = 6283;

        int sides = Game.R.nextInt(7) + 17;
        PolarPoint[] polPolars = new PolarPoint[sides];
        for (int nC = 0; nC < polPolars.length; nC++) {
            double r = (800 + Game.R.nextInt(200)) / 1000.0; //number between 0.8 and 1.0
            double theta = Game.R.nextInt(MAX_RADIANS_X1000) / 1000.0; // number between 0 and 6.283
            polPolars[nC] = new PolarPoint(r, theta);
        }

        setCartesians(polarToCartesian(
                Arrays.stream(polPolars)
                        .sorted(new Comparator<PolarPoint>() {
                            @Override
                            public int compare(PolarPoint p1, PolarPoint p2) {
                                return p1.getTheta().compareTo(p2.getTheta());
                            }
                        })
                        .collect(Collectors.toList()))
        );

    }


}
