package edu.uchicago.gerber._08final.mvc.model;

import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Floater extends Sprite {


	public Floater(Type type, int expiry, int radius, Color color) {

		super();
		setTeam(Team.FLOATER);
		setType(type);
		setExpiry(expiry);
		setRadius(radius);
		setColor(color);

		//set random DeltaX
		setDeltaX(somePosNegValue(15));

		//set random DeltaY
		setDeltaY(somePosNegValue(15));
		
		//set random spin
		setSpin(somePosNegValue(15));

		//random point on the screen
		setCenter(new Point(Game.R.nextInt(Game.DIM.width),
				Game.R.nextInt(Game.DIM.height)));

		//random orientation 
		setOrientation(Game.R.nextInt(360));

		//always set cartesian points last
		List<Point> pntCs = new ArrayList<>();
		pntCs.add(new Point(5, 5));
		pntCs.add(new Point(4,0));
		pntCs.add(new Point(5, -5));
		pntCs.add(new Point(0,-4));
		pntCs.add(new Point(-5, -5));
		pntCs.add(new Point(-4,0));
		pntCs.add(new Point(-5, 5));
		pntCs.add(new Point(0,4));

		setCartesians(pntCs);
	}

	@Override
	public void move() {
		super.move();
		//a floater spins
		setOrientation(getOrientation() + getSpin());
		//and it also expires
		expire();

	}

}
