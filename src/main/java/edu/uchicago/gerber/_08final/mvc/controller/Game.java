package edu.uchicago.gerber._08final.mvc.controller;

import edu.uchicago.gerber._08final.mvc.model.*;
import edu.uchicago.gerber._08final.mvc.view.GamePanel;


import javax.sound.sampled.Clip;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

// ===============================================
// == This Game class is the CONTROLLER
// ===============================================

public class Game implements Runnable, KeyListener {

    // ===============================================
    // FIELDS
    // ===============================================

    public static final Dimension DIM = new Dimension(1100, 900); //the dimension of the game.
    private GamePanel gmpPanel;
    //this is used throughout many classes.
    public static Random R = new Random();

    public final static int ANI_DELAY = 50; // milliseconds between screen
    // updates (animation)

    public final static int FRAMES_PER_SECOND = 1000 / ANI_DELAY;

    private Thread animationThread;
    private int level = 1;

    private boolean muted = true;


    private final int PAUSE = 80, // p key
            QUIT = 81, // q key
            LEFT = 37, // rotate left; left arrow
            RIGHT = 39, // rotate right; right arrow
            UP = 38, // thrust; up arrow
            SHIELD = 65,
            START = 83, // s key
            FIRE = 32, // space key
            MUTE = 77, // m-key mute
            SLOW = 87, // w slow motion power up
            ZAP = 90, // z, zap power up
            SPECIAL = 70;// fire special weapon;  F key

    private Clip clpThrust;
    private Clip clpMusicBackground;

    //list of floaters and attributes to spawn
    private static final Object[][] floaters = {{FRAMES_PER_SECOND * 30, Movable.Type.NEW_SHIP, 150, 50, Color.BLUE}, {FRAMES_PER_SECOND * 35, Movable.Type.SLOW_ASTEROID, 150, 50, Color.GREEN}, {FRAMES_PER_SECOND * 40, Movable.Type.SHIELD_BOOSTER, 150, 50, Color.RED}, {FRAMES_PER_SECOND * 45, Movable.Type.SUPER_BULLET, 150, 50, Color.PINK}, {FRAMES_PER_SECOND * 50, Movable.Type.ZAP_BOOSTER, 150, 50, Color.YELLOW}};

    //set wormhole and ufo frequency
    private static final int SPAWN_NEW_WORMHOLE = FRAMES_PER_SECOND * 30;
    private static final int SPAWN_NEW_UFO = FRAMES_PER_SECOND * 30;

    // ===============================================
    // ==CONSTRUCTOR
    // ===============================================

    public Game() {

        gmpPanel = new GamePanel(DIM);
        gmpPanel.addKeyListener(this);
        clpThrust = Sound.clipForLoopFactory("whitenoise.wav");
        clpMusicBackground = Sound.clipForLoopFactory("music-background.wav");


    }

    // ===============================================
    // ==METHODS
    // ===============================================

    public static void main(String args[]) {
        //typical Swing application main method
        EventQueue.invokeLater(new Runnable() { // uses the Event dispatch thread from Java 5 (refactored)
            public void run() {
                try {
                    Game game = new Game(); // construct itself
                    game.fireUpAnimThread();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void fireUpAnimThread() { // called initially
        if (animationThread == null) {
            animationThread = new Thread(this); // pass the thread a runnable object (this)
            animationThread.start();
        }
    }

    // implements runnable - must have run method
    public void run() {

        // lower this thread's priority; let the "main" aka 'Event Dispatch'
        // thread do what it needs to do first
        animationThread.setPriority(Thread.MIN_PRIORITY);

        // and get the current time
        long lStartTime = System.currentTimeMillis();

        // this thread animates the scene
        while (Thread.currentThread() == animationThread) {

            // spawn the extras
            spawnUFO();
            spawnNewFloaters();
            spawnWormhole();


            gmpPanel.update(gmpPanel.getGraphics()); // update takes the graphics context we must
            // surround the sleep() in a try/catch block
            // this simply controls delay time between
            // the frames of the animation

            // check for collisions or if all asteroids are dead
            checkCollisions();
            checkWormholeCollision();
            checkNewLevel();


            try {
                // The total amount of time is guaranteed to be at least ANI_DELAY long.  If processing (update)
                // between frames takes longer than ANI_DELAY, then the difference between lStartTime -
                // System.currentTimeMillis() will be negative, then zero will be the sleep time
                lStartTime += ANI_DELAY;
                Thread.sleep(Math.max(0,
                        lStartTime - System.currentTimeMillis()));
            } catch (InterruptedException e) {
                // do nothing (bury the exception), and just continue, e.g. skip this frame -- no big deal
            }
        } // end while
    } // end run

    private void checkCollisions() {

        Point pntFriendCenter, pntFoeCenter;
        int radFriend, radFoe;

        //This has order-of-growth of O(n^2), there is no way around this.
        for (Movable movFriend : CommandCenter.getInstance().getMovFriends()) {
            for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {

                pntFriendCenter = movFriend.getCenter();
                pntFoeCenter = movFoe.getCenter();
                radFriend = movFriend.getRadius();
                radFoe = movFoe.getRadius();

                //detect collision
                if (pntFriendCenter.distance(pntFoeCenter) < (radFriend + radFoe)) {
                    //remove the friend (so long as he is not protected)
                    if (!movFriend.isProtected() && CommandCenter.getInstance().getPowerUp() != Movable.Type.SHIELD_BOOSTER) {
                        CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
                    }
                    //remove the foe
                    CommandCenter.getInstance().getOpsList().enqueue(movFoe, CollisionOp.Operation.REMOVE);

                    // if it is a bullet or a shielded falcon...
                    if (!(movFriend instanceof Falcon) || CommandCenter.getInstance().getPowerUp() == Movable.Type.SHIELD_BOOSTER) {

                        // more points for a UFO
                        if (movFoe instanceof UFO) {
                            CommandCenter.getInstance().incScore(500);
                        } else {
                            CommandCenter.getInstance().incScore(200 - movFoe.getRadius());
                        }

                        // increment kills if collision is w/bullet
                        if (movFriend instanceof Bullet) {
                            CommandCenter.getInstance().incKills();
                        }

                    }
                    Sound.playSound("kapow.wav");

                }

            }//end if
        }//end inner for

        //check for collisions between falcon and floaters
        if (CommandCenter.getInstance().getFalcon() != null) {
            Point pntFalCenter = CommandCenter.getInstance().getFalcon().getCenter();

            int radFalcon = CommandCenter.getInstance().getFalcon().getRadius();
            Point pntFloaterCenter;
            int radFloater;

            for (Movable movFloater : CommandCenter.getInstance().getMovFloaters()) {
                pntFloaterCenter = movFloater.getCenter();
                radFloater = movFloater.getRadius();

                //detect collision
                if (pntFalCenter.distance(pntFloaterCenter) < (radFalcon + radFloater)) {
                    CommandCenter.getInstance().getOpsList().enqueue(movFloater, CollisionOp.Operation.REMOVE);

                    // increment the correct variable based on floater type
                    switch (movFloater.getType()) {
                        case NEW_SHIP:
                            CommandCenter.getInstance().incFalc();
                            break;
                        case SLOW_ASTEROID:
                            CommandCenter.getInstance().incSlowMotion();
                            break;
                        case SHIELD_BOOSTER:
                            CommandCenter.getInstance().incShield();
                            break;
                        case SUPER_BULLET:
                            CommandCenter.getInstance().incSuperBullet();
                            break;
                        case ZAP_BOOSTER:
                            CommandCenter.getInstance().incZap();
                            break;

                    }
                    Sound.playSound("pacman_eatghost.wav");
                }


            }//end if
        }//end inner for
        //end if not null

        processGameOpsQueue();

    }//end meth

    private void processGameOpsQueue() {

        //deferred mutation: these operations are done AFTER we have completed our collision detection to avoid
        // mutating the movable arraylists while iterating them above
        while (!CommandCenter.getInstance().getOpsList().isEmpty()) {
            CollisionOp cop = CommandCenter.getInstance().getOpsList().dequeue();
            Movable mov = cop.getMovable();
            CollisionOp.Operation operation = cop.getOperation();

            switch (mov.getTeam()) {
                case FOE:
                    if (operation == CollisionOp.Operation.ADD) {
                        CommandCenter.getInstance().getMovFoes().add(mov);
                    } else {
                        CommandCenter.getInstance().getMovFoes().remove(mov);
                        if (mov instanceof Asteroid) {
                            spawnSmallerAsteroids((Asteroid) mov);}
                        else if (mov instanceof UFO) {
                            createDebris(mov);

                        }

                    }

                    break;
                case FRIEND:
                    if (operation == CollisionOp.Operation.ADD) {
                        CommandCenter.getInstance().getMovFriends().add(mov);
                    } else {
                        CommandCenter.getInstance().getMovFriends().remove(mov);
                        if (mov instanceof Falcon)
                            CommandCenter.getInstance().spawnFalcon();
                    }
                    break;

                case FLOATER:
                    if (operation == CollisionOp.Operation.ADD) {
                        CommandCenter.getInstance().getMovFloaters().add(mov);
                    } else {
                        CommandCenter.getInstance().getMovFloaters().remove(mov);
                    }
                    break;

                case DEBRIS:
                    if (operation == CollisionOp.Operation.ADD) {
                        CommandCenter.getInstance().getMovDebris().add(mov);
                    } else {
                        CommandCenter.getInstance().getMovDebris().remove(mov);
                    }
                    break;


            }

        }
    }


    private void spawnSmallerAsteroids(Asteroid originalAsteroid) {

        //big asteroid
        if (originalAsteroid.getSize() == 0) {
            //spawn two medium Asteroids
            CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(originalAsteroid), CollisionOp.Operation.ADD);
            createDebris(originalAsteroid);
            CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(originalAsteroid), CollisionOp.Operation.ADD);

        }
        //medium size aseroid exploded
        else if (originalAsteroid.getSize() == 1) {
            //spawn three small Asteroids
            CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(originalAsteroid), CollisionOp.Operation.ADD);
            createDebris(originalAsteroid);
            CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(originalAsteroid), CollisionOp.Operation.ADD);
            createDebris(originalAsteroid);
            CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(originalAsteroid), CollisionOp.Operation.ADD);

        }

        //if it's a small asteroid, do nothing.
        else {
            createDebris(originalAsteroid);
        }
    }

    private void spawnNewFloaters() {
        // keep it from getting too "busy". one floater at a tiem
        if (CommandCenter.getInstance().getMovFloaters().size() == 0) {
            for (Object[] floater : floaters) {
                // more frequent as levels increase
                if ((System.currentTimeMillis() / ANI_DELAY) % ((int) floater[0] - level * 7L) == 0) {
                    CommandCenter.getInstance().getOpsList().enqueue(new Floater((Movable.Type) floater[1], (int) floater[2], (int) floater[3], (Color) floater[4]), CollisionOp.Operation.ADD);
                    break;
                }
            }
        }

    }

    // Called when user presses 's'
    private void startGame() {
        CommandCenter.getInstance().clearAll();
        CommandCenter.getInstance().initGame();
        CommandCenter.getInstance().setLevel(0);
        CommandCenter.getInstance().setPaused(false);

    }

    //this method spawns new asteroids
    private void spawnAsteroids(int nNum) {
        for (int nC = 0; nC < nNum; nC++) {
            //Asteroids with size of zero are big
            CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(0), CollisionOp.Operation.ADD);

        }
    }

    private void spawnWormhole() {
        // if recurrence time has been reached and there is not another active wormhole
        if (CommandCenter.getInstance().getActiveWormhole() == null && (System.currentTimeMillis() / ANI_DELAY) % SPAWN_NEW_WORMHOLE == 0) {
            // create a new wormhole and add set to active
            CommandCenter.getInstance().setActiveWormhole(new Wormhole());
        }
    }

    private void checkWormholeCollision() {
        // get active falcon and wormhole
        Wormhole activeWormhole = CommandCenter.getInstance().getActiveWormhole();
        Falcon activeFalcon = CommandCenter.getInstance().getFalcon();

        // if either is null, exit
        if (activeWormhole == null || activeFalcon == null) {
            return;
        }

        // get the radius of each
        int radFalcon = activeFalcon.getRadius();
        int radWormhole = activeWormhole.getRadius();

        // if collision...
        if (activeFalcon.getCenter().distance(activeWormhole.getInCenter()) < (radFalcon + radWormhole)) {
            // ... add to score
            CommandCenter.getInstance().incScore(500);

            //... reverse velocity and orientation
            activeFalcon.setDeltaX(-activeFalcon.getDeltaX());
            activeFalcon.setDeltaY(-activeFalcon.getDeltaY());
            activeFalcon.setOrientation((activeFalcon.getOrientation() + 180) % 360);

            // ... and move falcon center to the "out" wormhole
            CommandCenter.getInstance().getFalcon().setCenter(new Point(activeWormhole.getOutCenter().x, activeWormhole.getOutCenter().y));

            // lastly, get rid of wormhole (can only be used once)
            CommandCenter.getInstance().setActiveWormhole(null);

            // and play a noise
            Sound.playSound("shipspawn.wav");
        } else {
            // else, age wormhole
            activeWormhole.age();
        }
    }

    private void spawnUFO() {
        // if recurrence time has been reached and there is another floater, to prevent lags
        if ((System.currentTimeMillis() / ANI_DELAY) % SPAWN_NEW_UFO == 0 && CommandCenter.getInstance().getMovFloaters().size() == 0) {
            // create a new UFO in ops list
            CommandCenter.getInstance().getOpsList().enqueue(new UFO(CommandCenter.getInstance().getFalcon()), CollisionOp.Operation.ADD);
        }
    }

    private boolean isLevelClear() {
        //if there are no more Asteroids on the screen
        boolean asteroidFree = true;
        for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {
            if (movFoe instanceof Asteroid) {
                asteroidFree = false;
                break;
            }
        }
        return asteroidFree;
    }

    private void checkNewLevel() {

        if (isLevelClear() && !CommandCenter.getInstance().isGameOver()) {
            if (CommandCenter.getInstance().getFalcon() != null)
                //more asteroids at each level to increase difficulty
                spawnAsteroids(CommandCenter.getInstance().getLevel() + 2);
            CommandCenter.getInstance().setLevel(CommandCenter.getInstance().getLevel() + 1);

            if (CommandCenter.getInstance().getAccuracy() > 50) {
                gmpPanel.accuracyBonusExpiry = 100;
                CommandCenter.getInstance().incScore(500);
            }
            CommandCenter.getInstance().resetAccuracy();

        }
    }


    // Varargs for stopping looping-music-clips
    private static void stopLoopingSounds(Clip... clpClips) {
        for (Clip clp : clpClips) {
            clp.stop();
        }
    }

    private void createDebris(Movable origAsteroid) {
        // create a bunch of debris
        for (int i = 0; i < 10; i++) {
            CommandCenter.getInstance().getOpsList().enqueue(new Debris(origAsteroid), CollisionOp.Operation.ADD);
        }
    }

    // ===============================================
    // KEYLISTENER METHODS
    // ===============================================


    @Override
    public void keyPressed(KeyEvent e) {
        Falcon fal = CommandCenter.getInstance().getFalcon();
        int nKey = e.getKeyCode();

        if (nKey == START && CommandCenter.getInstance().isGameOver())
            startGame();

        if (fal != null) {

            switch (nKey) {
                case PAUSE:
                    CommandCenter.getInstance().setPaused(!CommandCenter.getInstance().isPaused());
                    if (CommandCenter.getInstance().isPaused())
                        stopLoopingSounds(clpMusicBackground, clpThrust);
                    break;

                case QUIT:
                    System.exit(0);
                    break;
                case UP:
                    fal.thrustOn();
                    if (!CommandCenter.getInstance().isPaused())
                        clpThrust.loop(Clip.LOOP_CONTINUOUSLY);
                    break;
                case LEFT:
                    fal.rotateLeft();
                    break;
                case RIGHT:
                    fal.rotateRight();
                    break;

                // if uses are available, use it and update power-up variables
                case SHIELD:

                    if (CommandCenter.getInstance().getShieldUses() > 0) {
                        CommandCenter.getInstance().setPowerUp(Movable.Type.SHIELD_BOOSTER);
                        CommandCenter.getInstance().setPowerUpExpiry(255);
                        CommandCenter.getInstance().setPowerUpDuration(255);
                        CommandCenter.getInstance().useShield();
                        Sound.playSound("shieldup.wav");
                    }
                    break;
                case SPECIAL:
                    if (CommandCenter.getInstance().getSuperBulletUses() > 0) {
                        CommandCenter.getInstance().setPowerUp(Movable.Type.SUPER_BULLET);
                        // shorter for super bullets, because they are very effective
                        CommandCenter.getInstance().setPowerUpExpiry(60);
                        CommandCenter.getInstance().setPowerUpDuration(60);
                        CommandCenter.getInstance().useSuperBullet();
                    }
                    break;

                case SLOW:
                    if (CommandCenter.getInstance().getSlowMotionUses() > 0) {
                        // slow down all foes by increasing speed factor
                        // speed of foes is divided by speed factor
                        CommandCenter.getInstance().setSpdFactor(4);
                        for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {
                            movFoe.setDeltaX(movFoe.getDeltaX() / CommandCenter.getInstance().getSpdFactor());
                            movFoe.setDeltaY(movFoe.getDeltaY() / CommandCenter.getInstance().getSpdFactor());
                        }

                        CommandCenter.getInstance().setPowerUp(Movable.Type.SLOW_ASTEROID);
                        CommandCenter.getInstance().setPowerUpExpiry(255);
                        CommandCenter.getInstance().setPowerUpDuration(255);
                        CommandCenter.getInstance().useSlowMotion();
                    }
                    break;

                case ZAP:
                    if (CommandCenter.getInstance().getZapUses() > 0) {
                        CommandCenter.getInstance().setPowerUp(Movable.Type.ZAP_BOOSTER);
                        CommandCenter.getInstance().setPowerUpExpiry(5);
                        CommandCenter.getInstance().useZap();
                        Sound.playSound("laser.wav");
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Falcon fal = CommandCenter.getInstance().getFalcon();
        int nKey = e.getKeyCode();
        //show the key-code in the console
        System.out.println(nKey);

        if (fal != null) {
            switch (nKey) {

                case FIRE:
                    int numBullets;
                    if (CommandCenter.getInstance().getPowerUp() == Movable.Type.SUPER_BULLET) {
                        numBullets = 4;
                    } else {
                        numBullets = 1;
                    }

                    for (int i = 0; i < numBullets; i++) {
                        CommandCenter.getInstance().getOpsList().enqueue(new Bullet(fal, 90 * i), CollisionOp.Operation.ADD);
                        CommandCenter.getInstance().incBullets();
                        Sound.playSound("laser.wav");
                    }

                    break;


                case LEFT:
                    fal.stopRotating();
                    break;
                case RIGHT:
                    fal.stopRotating();
                    break;
                case UP:
                    fal.thrustOff();
                    clpThrust.stop();
                    break;

                case MUTE:
                    if (!muted) {
                        stopLoopingSounds(clpMusicBackground);
                    } else {
                        clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
                    }
                    muted = !muted;
                    break;

                default:
                    break;
            }
        }
    }

    @Override
    // does nothing, but we need it b/c of KeyListener contract
    public void keyTyped(KeyEvent e) {
    }

}


