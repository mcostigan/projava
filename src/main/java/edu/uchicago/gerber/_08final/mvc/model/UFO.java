package edu.uchicago.gerber._08final.mvc.model;

import com.sun.codemodel.internal.JCommentPart;
import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class UFO extends Sprite{
    private Falcon falcon;
    public UFO(Falcon falcon) {
        super();
        this.falcon=falcon;

        setTeam(Team.FOE);

        setExpiry(250);
        setRadius(50);

        setOrientation(Game.R.nextInt(360));

        //always set cartesian points last
        List<Point> pntCs = new ArrayList<>();
        pntCs.add(new Point(5, 5));
        pntCs.add(new Point(4,0));
        pntCs.add(new Point(5, -5));
        pntCs.add(new Point(0,-4));
        pntCs.add(new Point(-5, -5));
        pntCs.add(new Point(-4,0));
        pntCs.add(new Point(-5, 5));
        pntCs.add(new Point(0,4));

        setCartesians(pntCs);
    }

    @Override
    public void move() {
        super.move();

        if (getExpiry()==0){
            CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
        }
        else {
            setExpiry(getExpiry()-1);

            int deltaX =  (falcon.getCenter().x-this.getCenter().x);
            int deltaY =  (falcon.getCenter().y-this.getCenter().y);

            double radians = Math.atan2(deltaY,deltaX);
            setDeltaX(Math.cos(radians)*5);
            setDeltaY(Math.sin(radians)*5);
        }
    }

    @Override
    public void draw(Graphics g) {
        super.draw(g);
        g.setColor(Color.GRAY);
        g.fillOval(getCenter().x-getRadius(),getCenter().y-getRadius(),getRadius()*2,getRadius()*2);

        g.setColor(Color.WHITE);
        g.fillOval(getCenter().x-getRadius()/2,getCenter().y-getRadius()/2,getRadius(),getRadius());

        g.setColor(Color.GREEN);
        g.fillOval(getCenter().x-getRadius()/4,getCenter().y-getRadius()/4,getRadius()/2,getRadius()/2);
        g.setColor(Color.WHITE);
    }
}
