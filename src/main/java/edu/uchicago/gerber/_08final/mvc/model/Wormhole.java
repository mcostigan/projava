package edu.uchicago.gerber._08final.mvc.model;

import edu.uchicago.gerber._08final.mvc.controller.Game;
import edu.uchicago.gerber._08final.mvc.view.GamePanel;

import java.awt.*;


public class Wormhole {
    // center of in and out holes
    private Point in;
    private Point out;

    // how long it lasts and how big it is
    private int expiry;
    private int radius;

    // drawing objects
    private final Stroke THICK_STROKE = new BasicStroke(5);
    private final Stroke NORMAL_STROKE =  new BasicStroke(1);

    public Wormhole() {
        this.in = new Point(Game.R.nextInt(Game.DIM.width),Game.R.nextInt(Game.DIM.height));
        this.out = new Point(Game.R.nextInt(Game.DIM.width),Game.R.nextInt(Game.DIM.height));
        this.expiry = 255;
        this.radius = 40;
    }

    public Point getInCenter(){
        return in;
    }

    public Point getOutCenter(){
        return out;
    }

    public int getRadius(){return radius;}

    public void draw(Graphics g){
        Graphics2D g2d = (Graphics2D) g;


        //draw an arc as a % of time remaining
        int arcLength = (int) (((double) expiry)/ ((double) 255) * 360);

        //Green at in
        g2d.setStroke(NORMAL_STROKE);
        g.setColor(Color.GREEN);
        g.drawOval(in.x-radius, in.y-radius,radius*2,radius*2);
        g2d.setStroke(THICK_STROKE);
        g.drawArc(in.x-radius-10, in.y-radius-10,radius*2+20,radius*2+20,0,arcLength);

        //point note
        g.drawString("+500",in.x-radius/2,in.y);


        // Red at out
        g2d.setStroke(NORMAL_STROKE);
        g.setColor(Color.RED);
        g.drawOval(out.x-radius, out.y-radius,radius*2,radius*2);
        g2d.setStroke(THICK_STROKE);
        g.drawArc(out.x-radius-10, out.y-radius-10,radius*2+20,radius*2+20,0,arcLength);
        g2d.setStroke(NORMAL_STROKE);

    }
    public void age(){
        expiry--;
        if (expiry==0){
            CommandCenter.getInstance().setActiveWormhole(null);
        }
    }

}
