package edu.uchicago.gerber._08final.mvc.model;

import java.awt.*;

public interface Movable {

	enum Team {
		FRIEND, FOE, FLOATER, DEBRIS
	}

	enum Type {
		NEW_SHIP,SLOW_ASTEROID, SHIELD_BOOSTER, SUPER_BULLET, ZAP_BOOSTER
	}
	//for the game to move and draw movable objects
	void move();
	void draw(Graphics g);

	//for collision detection
	Point getCenter();
	int getRadius();
	Team getTeam();
	Type getType();
	void setDeltaX(double d);
	void setDeltaY(double d);
	double getDeltaX();
	double getDeltaY();
	boolean isProtected();


} //end Movable
