package edu.uchicago.gerber._08final.mvc.model;


import edu.uchicago.gerber._08final.mvc.controller.Sound;
import lombok.Data;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

//the lombok @Data gives us automatic getters and setters on all members
@Data
public class CommandCenter {

    private int numFalcons;
    private int level;

    private int bullets;
    private int kills;
    private long score;

    // variables to store number of uses of different powerups
    private int shieldUses;
    private int slowMotionUses;
    private int superBulletUses;
    private int zapUses;

    // the speed factor of a newly created asteroid
    // used to slowdown asteroids during the slow-motion power-up
    private int spdFactor = 1;

    // store which powerUp is being used, how long they last, and how much time is left
    private Movable.Type powerUp;
    private int powerUpDuration;
    private int powerUpExpiry;

    //the falcon is located in the movFriends array, but since we use this reference a lot, we keep track of it in a
    //separate reference. See spawnFalcon() method below.
    private Falcon falcon;
    private boolean paused;

    private List<Movable> movDebris = new LinkedList<>();
    private List<Movable> movFriends = new LinkedList<>();
    private List<Movable> movFoes = new LinkedList<>();
    private List<Movable> movFloaters = new LinkedList<>();

    private Wormhole activeWormhole;

    private GameOpsList opsList = new GameOpsList();


    private static CommandCenter instance = null;

    // Constructor made private - static Utility class only
    private CommandCenter() {
    }


    public static CommandCenter getInstance() {
        if (instance == null) {
            instance = new CommandCenter();
        }
        return instance;
    }


    public void initGame() {
        setLevel(1);
        setScore(0);

        setShieldUses(1);
        setSlowMotionUses(1);
        setSuperBulletUses(1);
        setZapUses(1);

        setActiveWormhole(null);
        setPowerUp(null);

        setBullets(0);
        setKills(0);
        setNumFalcons(4);
        spawnFalcon();

    }

    public void spawnFalcon() {

        falcon = new Falcon();
        opsList.enqueue(falcon, CollisionOp.Operation.ADD);
        setNumFalcons(getNumFalcons() - 1);
        Sound.playSound("shipspawn.wav");

    }

    public void clearAll() {
        movDebris.clear();
        movFriends.clear();
        movFoes.clear();
        movFloaters.clear();
    }

    public void incScore(int increment) {
        score += increment;
    }


    // increment number of falcons when new ship floater picked up
    public void incFalc() {
        numFalcons++;
    }

    public boolean isGameOver() {        //if the number of falcons is zero, then game over
        return getNumFalcons() == 0;
    }

    // count of bullets fired, for accurracy
    public void incBullets() {
        bullets += 1;
    }

    public void incKills() {
        kills += 1;
    }

    public double getAccuracy() {
        return Math.round(kills * 100.0 / bullets);
    }

    public void resetAccuracy() {
        kills = 0;
        bullets = 0;
    }

    // variables for the use of power-ups
    public void incShield() {
        shieldUses++;
    }

    public void useShield() {
        shieldUses--;
    }

    public void incSlowMotion() {
        slowMotionUses++;
    }

    public void useSlowMotion() {
        slowMotionUses--;
    }

    public void incSuperBullet() {
        superBulletUses++;
    }

    public void useSuperBullet() {
        superBulletUses--;
    }

    public void incZap() {
        zapUses++;
    }

    public void useZap() {
        zapUses--;
    }

    // age the power-up by one frame
    public void agePowerUp() {
        // decrement the expiration
        powerUpExpiry--;

        // if expired, set power-up to null
        if (powerUpExpiry <= 0) {
            // and if slow, then speed everything back up
            if (powerUp == Movable.Type.SLOW_ASTEROID) {
                for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {
                    movFoe.setDeltaX(movFoe.getDeltaX() * CommandCenter.getInstance().getSpdFactor());
                    movFoe.setDeltaY(movFoe.getDeltaY() * CommandCenter.getInstance().getSpdFactor());

                }
                CommandCenter.getInstance().setSpdFactor(1);
            }
            powerUp = null;
        }
    }
}
