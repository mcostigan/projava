package edu.uchicago.gerber._08final.mvc.view;

import edu.uchicago.gerber._08final.mvc.controller.Game;
import edu.uchicago.gerber._08final.mvc.model.CommandCenter;
import edu.uchicago.gerber._08final.mvc.model.Falcon;
import edu.uchicago.gerber._08final.mvc.model.Movable;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.ImageObserver;
import java.io.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Stream;


public class GamePanel extends Panel {

    // ==============================================================
    // FIELDS
    // ==============================================================

    // The following "off" vars are used for the off-screen double-bufferred image.
    private Dimension dimOff;
    private Image imgOff;
    private Graphics grpOff;

    private GameFrame gmf;
    private Font fnt = new Font("SansSerif", Font.BOLD, 12);
    private Font fntBig = new Font("SansSerif", Font.BOLD + Font.ITALIC, 36);
    private FontMetrics fmt;
    private int fontWidth;
    private int fontHeight;
    private String strDisplay = "";

    //accuracy bonus notification pop-up
    public int accuracyBonusExpiry;


    // ==============================================================
    // CONSTRUCTOR
    // ==============================================================

    public GamePanel(Dimension dim) {
        gmf = new GameFrame();
        gmf.getContentPane().add(this);
        gmf.pack();
        initView();

        gmf.setSize(dim);
        gmf.setTitle("Game Base");
        gmf.setResizable(false);
        gmf.setVisible(true);
        this.setFocusable(true);
    }


    // ==============================================================
    // METHODS
    // ==============================================================

    private void drawScore(Graphics g) {
        g.setColor(Color.white);
        g.setFont(fnt);

        g.drawString("SCORE :  " + CommandCenter.getInstance().getScore(), fontWidth, fontHeight);
        g.drawString("LEVEL : " + CommandCenter.getInstance().getLevel(), fontWidth, fontHeight * 2);
        g.drawString("ACCURACY : " + CommandCenter.getInstance().getAccuracy() + "%", fontWidth, fontHeight * 3);
        if (accuracyBonusExpiry>0){
            g.setColor(Color.GREEN);
            g.drawString("+500",fontWidth*7, fontHeight * 3);
            accuracyBonusExpiry--;
            g.setColor(Color.WHITE);
        }
        g.drawString("SHIELD (A): " + CommandCenter.getInstance().getShieldUses(), fontWidth, fontHeight * 4);
        g.drawString("SLOW MOTION (W) : " + CommandCenter.getInstance().getSlowMotionUses(), fontWidth, fontHeight * 5);
        g.drawString("SUPER BULLETS (F) : " + CommandCenter.getInstance().getSuperBulletUses(), fontWidth, fontHeight * 6);
        g.drawString("ZAPS (Z) : " + CommandCenter.getInstance().getZapUses(), fontWidth, fontHeight * 7);
    }


    @SuppressWarnings("unchecked")
    public void update(Graphics g) {
        if (grpOff == null || Game.DIM.width != dimOff.width
                || Game.DIM.height != dimOff.height) {
            dimOff = Game.DIM;
            imgOff = createImage(Game.DIM.width, Game.DIM.height);
            grpOff = imgOff.getGraphics();
        }
        // Fill in background with black.
        grpOff.setColor(Color.black);
        grpOff.fillRect(0, 0, Game.DIM.width, Game.DIM.height);

        drawScore(grpOff);

        if (CommandCenter.getInstance().isGameOver()) {
            displayTextOnScreen();
        } else if (CommandCenter.getInstance().isPaused()) {
            strDisplay = "Game Paused";
            grpOff.drawString(strDisplay,
                    (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4);
        }

        //playing and not paused!
        else {
            iterateMovables(grpOff,
                    CommandCenter.getInstance().getMovDebris(),
                    CommandCenter.getInstance().getMovFloaters(),
                    CommandCenter.getInstance().getMovFoes(),
                    CommandCenter.getInstance().getMovFriends());

            if (CommandCenter.getInstance().getActiveWormhole() != null) {
                CommandCenter.getInstance().getActiveWormhole().draw(grpOff);
            }

            drawNumberShipsLeft(grpOff);


        }

        //draw the double-Buffered Image to the graphics context of the panel
        g.drawImage(imgOff, 0, 0, this);
    }


    //for each movable array, process it.
    @SafeVarargs
    private final void iterateMovables(final Graphics g, List<Movable>... arrayOfListMovables) {

        BiConsumer<Graphics, Movable> moveDraw = (grp, mov) -> {
            mov.move();
            mov.draw(grp);
        };

        Arrays.stream(arrayOfListMovables)
                .flatMap(Collection::stream)
                .forEach(m -> moveDraw.accept(g, m));


    }


    private void drawNumberShipsLeft(Graphics g) {
        int numFalcons = CommandCenter.getInstance().getNumFalcons();
        while (numFalcons > 0) {
            drawOneShipLeft(g, numFalcons--);
        }
    }

    // Draw the number of falcons left. Upside-down, but ok.
    private void drawOneShipLeft(Graphics g, int offSet) {
        Falcon falcon = CommandCenter.getInstance().getFalcon();

        //EDIT:
        //If only one ship left, red, If two->yellow, else->green
        switch (CommandCenter.getInstance().getNumFalcons()) {
            case 1:
                g.setColor(Color.red);
                break;
            case 2:
                g.setColor(Color.yellow);
                break;
            default:
                g.setColor(Color.green);
        }

        g.drawPolygon(
                Arrays.stream(falcon.getCartesians())
                        .map(pnt -> pnt.x + fontWidth)
                        .mapToInt(Integer::intValue)
                        .toArray(),

                Arrays.stream(falcon.getCartesians())
                        .map(pnt -> pnt.y + fontHeight * 8)
                        .mapToInt(Integer::intValue)
                        .toArray(),

                falcon.getCartesians().length);
        g.drawString("x" + CommandCenter.getInstance().getNumFalcons(), fontWidth + 20, fontHeight * 8);


    }

    private void initView() {
        Graphics g = getGraphics();            // get the graphics context for the panel
        g.setFont(fnt);                        // take care of some simple font stuff
        fmt = g.getFontMetrics();
        fontWidth = fmt.getMaxAdvance();
        fontHeight = fmt.getHeight();
        g.setFont(fntBig);                    // set font info
    }

    // This method draws some text to the middle of the screen before/after a game
    private void displayTextOnScreen() {

        strDisplay = "GAME OVER";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4);

        strDisplay = "use the arrow keys to turn and thrust";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + fontHeight + 40);

        strDisplay = "use the space bar to fire";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + fontHeight + 80);

        strDisplay = "'S' to Start";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + fontHeight + 120);

        strDisplay = "'P' to Pause";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + fontHeight + 160);

        strDisplay = "'Q' to Quit";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + fontHeight + 200);
        strDisplay = "'A' for Shield (when available)";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + fontHeight + 240);

        strDisplay = "'W' for Slow Motion (when available)";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + fontHeight + 280);

        strDisplay = "'F' for Super Bullets (when available)";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + fontHeight + 320);

        strDisplay = "'Z' for Zap (when available)";
        grpOff.drawString(strDisplay,
                (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
                        + fontHeight + 360);
    }


}