INSTRUCTIONS:

Hello, Jonathan!

Welcome to my asteroids game. I did a pretty straightforward extension of Professor Gerber's game. I'll outline the changes I implemented below:

Scoring:
    Obviously, you get points for hitting an asteroid. Each asteroid you hit increases your score by 200 - the radius of the asteroid. This means that big asteroids are worth 100 points, medium are worth 150, and small are worth 175.
    So to hit a big asteroid, both of its children, and all of its grandchildren will net you 100 + 2*(150) + 3*2*(175) = 1450 points.

    Asteroids break up when they hit you, but you don't get points for that (unless you are wearing your shield, in which case, go bowling and have fun with it)

     I tried to implement scoring in a way that encourages you to play the game in a fun way. So here are a few fun quirks:
        ACCURACY BONUS:
        It's easy to spin around and to go to town on your space bar. It's harder to be accurate. For each level, you get a 500 point bonus if your accuracy exceeds 50%.
        Accuracy is calculated as kills/bullets and tracked in the top left corner. It is possible (though rare) to kill two asteroids with one laser, meaning an accuracy may exceed 100% at time. Do with that what you may.

        WORMHOLE BONUS:
        It's also tempting to stay stationary, since the ship is a bit hard to control, with the absence of friction and what not. But flying around is more exciting.
        So, periodically, a wormhole will popup. There will be two holes an "in" and an "out", green and red, respectively. The meter around the wormholes tell you how much longer before they expire. Fly your ship into the green and be transported to the red.
        It takes a skilled pilot to navigate a wormhole in one piece, so you'll get 500 points for your efforts.

        UFOs:
        I implemented a UFO (gray, white, and green circles) along with Professor Gerber's tutorial. Hit a UFO, and you get 500 points. Nothing too complicated on this one.

     There are also a number of floaters and power-ups that will appear sporadically (colored polygons). Catch them for special bonus.
        New Ship (BLUE) - This one gives you an extra life.
        Shield (RED) (A) - Turn your shield on and go bowling. You get points for everything you crash into, so have fun
        Slow Motion (GREEN) (W)- slow down all of your enemies for a short period. They'll be like fish in a barrel out there.
        Super Bullets (PINK) (F)- Everytime you press the space bar, you will shoot a bullet at 0, 90, 180, and 270 degrees. This power-up is quick, because it is JUST NOT FAIR (may I recommend spinning while you do it?)
        ZAP (YELLOW) (Z) - My personal favorite, press Z when a few asteroids are closing in, and you'll zap everything within a certain radius.
            Zap a big asteroid, which will spawn smaller asteroids, and if those spawn in your radius, zap those too. You get points for all of this, so I recommend using them on larger ones to accumulate 1450 points really quick.

     Like the wormholes, the duration of the power-up is tracked in the meter that surrounds the ship after you activate it.
     For practical grading purposes, you'll start the game with one of each power-up, so you can test without having to chase down the floaters.

     That's just about everything. I hope you enjoy.

