package edu.uchicago.gerber._03objects.P8_5;

public class SodaCanDriver {
    public static void main(String[] args) {
        SodaCan mySodaCan = new SodaCan(1,2);
        System.out.println(mySodaCan.getSurfaceArea());
        System.out.println(mySodaCan.getVolume());
    }
}
