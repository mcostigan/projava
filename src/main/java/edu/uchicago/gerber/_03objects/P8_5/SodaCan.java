package edu.uchicago.gerber._03objects.P8_5;

public class SodaCan {
    private double dblHeight;
    private double dblRadius;

    public SodaCan(int dblHeight, int dblRadius) {
        this.dblHeight = dblHeight;
        this.dblRadius = dblRadius;
    }

    /**
     * Calculates the surface area of the soda can using radius and height
     * @return surface area of can
     */
    public double getSurfaceArea() {
        // 2πrh+2πr^2
        return 2*Math.PI*dblRadius*dblHeight + 2*Math.PI*dblRadius*dblRadius;
    }

    /**
     * Calculates the volume of the soda can instance using radius and height
     * @return volume of soda can
     */
    public double getVolume() {
        // πr2h
        return Math.PI*dblRadius*dblRadius*dblHeight;
    }
}
