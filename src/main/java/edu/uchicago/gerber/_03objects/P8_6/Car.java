package edu.uchicago.gerber._03objects.P8_6;

public class Car {
    private double fuelEfficiency;
    private double gasLevel;

    /**
     * construct the car with given fuel efficienct
     * @param fuelEfficiency in miles/gallon
     */
    public Car(double fuelEfficiency) {
        this.fuelEfficiency = fuelEfficiency;
    }

    /**
     * drive a given number of miles and update gas level
     * @param miles driving distance
     */
    public void drive(double miles){
        // distance in miles divided by fuel efficiency in mpg = gallons used
        // subtract from gas level
        gasLevel -= miles/fuelEfficiency;
    }

    /**
     * Read gas level to the user
     * @return gasLevel of this instance of the car
     */
    public double getGasLevel() {
        return gasLevel;
    }

    /**
     * give the car gas
     * @param gasAdded - gallons of gase added to car
     */
    public void addGas(double gasAdded) {
        this.gasLevel += gasAdded;
    }
}
