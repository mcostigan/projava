package edu.uchicago.gerber._03objects.P8_8;

public class VotingDriver {
    public static void main(String[] args) {
        VotingMachine myVote = new VotingMachine();

        for (int i = 0; i < 100; i++) {
            if (i%2==0){
                myVote.voteDemocrat();
            } else  {
                myVote.voteRepublican();
            }
            System.out.println("R:"+myVote.getVotesRep()+";D:"+myVote.getVotesDem());
        }

        myVote.clear();
        System.out.println(myVote.getVotesDem());

    }
}
