package edu.uchicago.gerber._03objects.P8_8;

public class VotingMachine {
    private int votesDem;
    private int votesRep;

    /**
     * @return # of votes for democrat
     */
    public int getVotesDem() {
        return votesDem;
    }

    /**
     * add a vote for a democrat
     */
    public void voteDemocrat() {
        this.votesDem += 1;
    }

    /**
     * @return # of votes for republicans
     */
    public int getVotesRep() {
        return votesRep;
    }

    /**
     * add a vote for a republican
     */
    public void voteRepublican() {
        this.votesRep += 1;
    }

    /**
     * clear votes
     */
    public void clear() {
        votesDem = 0;
        votesRep = 0;
    }
}
