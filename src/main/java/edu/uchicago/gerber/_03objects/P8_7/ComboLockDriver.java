package edu.uchicago.gerber._03objects.P8_7;

public class ComboLockDriver {
    public static void main(String[] args) {
        // Combo lock test
        ComboLock myLock = new ComboLock(15,12,25);
        myLock.turnRight(65);
        System.out.println(myLock.getDial());
        myLock.turnLeft(37);
        System.out.println(myLock.getDial());
        myLock.turnRight(27);
        System.out.println(myLock.getDial());
        System.out.println(myLock.open());
    }
}
