package edu.uchicago.gerber._03objects.P8_7;

public class ComboLock {
    private int dial;
    private String strKey;
    private String strHistory;

    public ComboLock(int secret1, int secret2, int secret3) throws IllegalArgumentException {
        if (secret1 > 39 || secret2 > 39 || secret3 > 39 || secret1 < 0 || secret2 < 0 || secret3 < 0) {
            throw new IllegalArgumentException("The combination can only consist of numbers from 0 to 39, inclusive");
        }

        // Create string key
        strKey = "Right" + secret1 + "|Left" + secret2 + "|Right" + secret3 + "|";

        // Clear history string
        strHistory = "";
    }

    public void reset() {
        dial = 0;
        strHistory = "";
    }

    public void turnRight(int ticks) {
        // Move right
        dial = dial-ticks;

        // Add 40 until dial is non-negative
        while (dial<0){
            dial+=40;
        }

        // log in history
        strHistory += "Right" + dial + "|";
    }

    public void turnLeft(int ticks) {
        dial = (dial + ticks) % 40;
        strHistory += "Left" + dial + "|";
    }

    public String open() {
        if (strHistory.equals(strKey)) {
            this.reset();
            return "Open";
        } else {
            this.reset();
            return "No";
        }

    }

    public int getDial() {
        return dial;
    }
}
