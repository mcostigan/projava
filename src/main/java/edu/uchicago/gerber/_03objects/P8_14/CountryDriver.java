package edu.uchicago.gerber._03objects.P8_14;

import java.util.ArrayList;

public class CountryDriver {
    public static void main(String[] args) {
        ArrayList<Country> countries = new ArrayList<Country>();

        countries.add(new Country("Monaco", 38682, 1));
        countries.add(new Country("Singapore", 5757499, 276));
        countries.add(new Country("Hong Kong", 7371730, 426));
        countries.add(new Country("Gibraltar", 33718, 2));
        countries.add(new Country("Bahrain", 1501635, 303));
        analyzeCountries(countries);
    }

    public static void analyzeCountries(ArrayList<Country> countries) {
        String strMaxDensity = "";
        double dblMaxDensity = 0;
        String strMaxPop = "";
        int intMaxPop = 0;
        String strMaxArea ="";
        double dblMaxArea = 0;

        for (Country country : countries) {
            double thisArea = country.getArea();
            int thisPop = country.getPopulation();
            double thisDensity = country.getPopulationDensity();
            String thisName = country.getName();

            if (thisArea > dblMaxArea) {
                dblMaxArea = thisArea;
                strMaxArea = thisName;
            }

            if (thisPop > intMaxPop) {
                intMaxPop = thisPop;
                strMaxPop = thisName;
            }

            if (thisDensity > dblMaxDensity) {
                dblMaxDensity = thisDensity;
                strMaxDensity = thisName;
            }

        }

        System.out.println("Largest Area: " + strMaxArea + " (" + dblMaxArea + ")");
        System.out.println("Largest Population:" + strMaxPop + " (" + intMaxPop + ")");
        System.out.println("Largest Population Density:" + strMaxDensity + " (" + dblMaxDensity + ")");
    }
}
