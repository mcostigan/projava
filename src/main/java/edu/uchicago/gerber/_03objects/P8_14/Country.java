package edu.uchicago.gerber._03objects.P8_14;

public class Country {
    private int population;
    private double area;
    private double populationDensity;
    private String name;

    /**
     * Construct country with given attributes
     * @param name
     * @param population
     * @param area
     */
    public Country(String name,int population, double area) {
        this.population = population;
        this.area = area;
        this.name = name;
        this.populationDensity = this.population/this.area;
    }

    /**
     *
     * @return population of country
     */
    public int getPopulation() {
        return population;
    }

    /**
     *
     * @return area of country
     */
    public double getArea() {
        return area;
    }

    /**
     *
     * @return name of country
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return population of country
     */
    public double getPopulationDensity() {
        return populationDensity;
    }
}
