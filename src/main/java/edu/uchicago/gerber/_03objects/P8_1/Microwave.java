package edu.uchicago.gerber._03objects.P8_1;

public class Microwave {
    private int intTime;
    private int intLevel = 1;

    /**
     * add 30 seconds to timer
     */
    public void add30() {
        this.intTime = intTime + 30;
    }

    /**
     * Set to level 1
     */
    public void setLevelOne() {
        this.intLevel = 1;
    }

    /**
     * Set to level 2
     */
    public void setLevelTwo() {
        this.intLevel = 2;
    }

    public void reset() {
        this.intTime = 0;
        this.intLevel = 1;
    }

    ;

    /**
     * Start microwave
     */
    public void start() {
        System.out.println("Cooking for " + intTime + " seconds at level " + intLevel);
    }
}
