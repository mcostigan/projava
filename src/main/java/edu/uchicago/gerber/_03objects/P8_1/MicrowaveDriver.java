package edu.uchicago.gerber._03objects.P8_1;

public class MicrowaveDriver {
    public static void main(String[] args) {
        // Test microwave class
        Microwave myMicrowave = new Microwave();
        myMicrowave.add30();
        myMicrowave.add30();
        myMicrowave.start();
        myMicrowave.reset();
        myMicrowave.start();
    }
}
