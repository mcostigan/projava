package edu.uchicago.gerber._03objects.P8_19;

public class Cannonball {
    private double xPosition;
    private double yPosition;
    private double xVelocity;
    private double yVelocity;

    /**
     * construct cannonball with X position
     * @param xPosition
     */
    public Cannonball(double xPosition) {
        this.xPosition = xPosition;
        yPosition = 0;
        xVelocity = 0;
        yVelocity = 0;
    }

    /**
     * let the cannonball fly for a given number of seconds
     * @param sec - length of time to let the cannonball fly
     */
    public void move(double sec){
        // add horizontal displacement to xposition
        xPosition += xVelocity*sec;
        // add vertical displacement to yposition
        yPosition += yVelocity*sec;
        // gravity accelerates and -9.81 m/s^2
        yVelocity += -9.81*sec;
    }

    /**
     *
     * @return current x position
     */
    public double getX() {
        return xPosition;
    }

    /**
     *
     * @return current y position
     */
    public double getY() {
        return yPosition;
    }

    /**
     * shoot the cannonball
     * display x and y positions after each .1 seconds
     * @param angle - launch trajectory
     * @param initialVelocity - launch velocity
     */
    public void shoot(double angle, double initialVelocity){
        // calculate velocities
        xVelocity = initialVelocity*Math.cos(angle);
        yVelocity = initialVelocity*Math.sin(angle);

        // move cannonball until it comes back down to earth
        do {
            move(.1);
            System.out.println("(" + getX() +", " + getY() + ")");
        } while (yPosition>0);

    }
}
