package edu.uchicago.gerber._03objects.P8_16;

import java.util.ArrayList;

public class Mailbox {
    // Mailbox is array list of messages
    private ArrayList<Message> myMessages = new ArrayList<Message>();

    /**
     *
     * @param m - message to add to mailbox
     */
    public void addMessage(Message m){
        myMessages.add(m);
    }

    /**
     *
     * @param i - index of message to return
     * @return  message of index i
     * @throws IllegalArgumentException if index is out of bounds
     */
    public Message getMessage(int i) throws IllegalArgumentException{
        if (i<0 || i > myMessages.size()-1){
            throw new IllegalArgumentException("Message [" + i +"] does not exist");
        }
        return myMessages.get(i);
    }

    /**
     *
     * @param i index of message to delete
     * @throws IllegalArgumentException if index is out of bounds
     */
    public void deleteMessage(int i) throws IllegalArgumentException{
        if (i<0 || i > myMessages.size()-1){
            throw new IllegalArgumentException("Message [" + i +"] does not exist");
        }
        myMessages.remove(i);
    }
}
