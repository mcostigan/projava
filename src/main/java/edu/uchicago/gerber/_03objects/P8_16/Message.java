package edu.uchicago.gerber._03objects.P8_16;

public class Message {
    private String recipient;
    private String sender;
    private StringBuilder message = new StringBuilder();

    public Message(String sender, String recipient) {
        this.recipient = recipient;
        this.sender = sender;
    }

    public void append(String toAppend) {
        if (message.length() == 0) {
            message.append(toAppend);
        } else {
            message.append('\n'+toAppend);
        }
    }

    @Override
    public String toString() {
        return
                "----------\n"+
                "To: " + recipient + '\n' +
                "----------\n"+
                "From: '" + sender + '\n' +
                "----------\n"+
                "Message: " + message+
                "\n----------";
    }
}
