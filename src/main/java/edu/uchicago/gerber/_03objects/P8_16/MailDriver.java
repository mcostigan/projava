package edu.uchicago.gerber._03objects.P8_16;

public class MailDriver {
    public static void main(String[] args) {
        Message myMessage = new Message("Matt","Jonathan");
        myMessage.append("Hello there");
        myMessage.append("How are you?");
        myMessage.append("Do you like grading?");

        System.out.println(myMessage);

        Mailbox myMailbox = new Mailbox();
        myMailbox.addMessage(myMessage);

        try {
        System.out.println(myMailbox.getMessage(0));
        myMailbox.deleteMessage(0);
        myMailbox.getMessage(0);}
        catch (IllegalArgumentException e ) {
            System.out.println(e.getMessage());
        }
    }
}
