package edu.uchicago.gerber._04interfaces.R9_8;

public class Vehicle {
}

class Car extends Vehicle{

}

class Truck extends Vehicle{

}
class Sedan extends Car{

}
class Coupe extends Car{

}
class PickupTruck extends Car{

}
class SportUtilityVehicle extends Car{}
class Minivan extends Car{}
class Bicycle extends Vehicle{}
class Motorcycle extends Bicycle{}
