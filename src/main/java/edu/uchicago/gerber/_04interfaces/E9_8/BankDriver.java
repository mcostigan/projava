package edu.uchicago.gerber._04interfaces.E9_8;

public class BankDriver {
    public static void main(String[] args) {
        BasicAccount myAccount = new BasicAccount();
        System.out.println(myAccount.getBalance());
        myAccount.deposit(5000);
        System.out.println(myAccount.getBalance());
        myAccount.withdrawal(5001);
        System.out.println(myAccount.getBalance());
    }
}
