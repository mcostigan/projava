package edu.uchicago.gerber._04interfaces.E9_8;

public class BasicAccount extends BankAccount{
    @Override
    public void withdrawal(double amount) {
        super.withdrawal(Math.min(amount,super.getBalance()));
    }
}
