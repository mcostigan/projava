package edu.uchicago.gerber._04interfaces.E9_17;

public class SodaCan implements Measurable {
    private double dblHeight;
    private double dblRadius;
    private double dblArea;
    private double dblVolume;

    public SodaCan(int dblHeight, int dblRadius) {
        this.dblHeight = dblHeight;
        this.dblRadius = dblRadius;
        this.dblArea = 2*Math.PI*dblRadius*dblHeight + 2*Math.PI*dblRadius*dblRadius;
        this.dblVolume = Math.PI*dblRadius*dblRadius*dblHeight;
    }

    /**
     * Calculates the surface area of the soda can using radius and height
     * @return surface area of can
     */
    public double getSurfaceArea() {
        return dblArea;
    }

    /**
     * Calculates the volume of the soda can instance using radius and height
     * @return volume of soda can
     */
    public double getVolume() {
        return dblVolume;
    }

    public double getMeasure(){
        return dblArea;
    }
}
