package edu.uchicago.gerber._04interfaces.E9_17;

import java.awt.*;
import java.lang.reflect.Array;

public class SodaDriver {
    public static void main(String[] args) {
        SodaCan[] cans = {new SodaCan(1,1),new SodaCan(2,3),new SodaCan(5,8),new SodaCan(13,21)};
        Measurable m = null;
        for (SodaCan can : cans) {
            System.out.println(can.getSurfaceArea());
        }
        System.out.println(Measurable.average(cans));

    }


}
