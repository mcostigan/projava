package edu.uchicago.gerber._04interfaces.E9_13;

import java.awt.*;

public class RectDriver {
    public static void main(String[] args) {
        BetterRectangle myRect = new BetterRectangle(5,5,11,10);
        System.out.println(myRect.getArea());
        System.out.println(myRect.getPerimeter());
    }

}
