package edu.uchicago.gerber._04interfaces.P9_6;

import java.util.ArrayList;
import java.util.Scanner;

public class ApptDriver {
    public static void main(String[] args) {

        Appointment dailyLunch = new Daily("lunch",2021,10,23);
        Appointment monthlyDoctor = new Monthly("doctor",2021,10,1);
        Appointment onetimeAppt = new OneTime("doctor",2021,10,15);

        ArrayList<Appointment> appointments = new ArrayList<Appointment>();
        appointments.add(new Daily("lunch",2021,10,23));
        appointments.add(new Monthly("doctor",2021,10,1));
        appointments.add(new OneTime("doctor",2021,10,15));

        Scanner in =  new Scanner(System.in);
        try {
        System.out.println("What date do you want to query? (mm/dd/yyyy) ");
        String strDate = in.nextLine();
        String[] date = strDate.split("/");
        int month = Integer.parseInt(date[0]);
        int day = Integer.parseInt(date[1]);
        int year = Integer.parseInt(date[2]);
        if (month>12 || month<1){
            throw new IllegalArgumentException("Month must be between 1-12");
        }
        if (day>31 || day<1){
            throw new IllegalArgumentException("Day must be between 1-31");
        }
        occursOn(year,month,day,appointments);
        }
        catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());

        }


    }

    public static void occursOn(int year, int month, int day, ArrayList<Appointment> appointments){
        for (Appointment appointment : appointments) {
            if (appointment.occursOn(year,month,day)){
                System.out.println(appointment.getAppointment());
            }

        }
    }
}

