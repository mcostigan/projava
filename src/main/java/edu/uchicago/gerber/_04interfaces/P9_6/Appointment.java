package edu.uchicago.gerber._04interfaces.P9_6;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public abstract class Appointment {
    private String description;
    private int timeStamp;
    private int year;
    private int month;
    private int day;

    public Appointment(String strAppointment, int year, int month, int day) {
        this.timeStamp = year*365+month*31+day;
        this.year=year;
        this.month=month;
        this.day=day;
        this.description = strAppointment;
    }

    public boolean occursOn(int year, int month, int day){
        return this.year==year && this.month==month && this.day==day;
    }

    public String getAppointment() {
        return description;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getTimeStamp() {
        return timeStamp;
    }
}
