package edu.uchicago.gerber._04interfaces.P9_6;


import java.util.ArrayList;

public class Daily extends Appointment {
    public Daily(String strAppointment, int intYear, int intMonth, int intDay) {
        super(strAppointment, intYear, intMonth, intDay);
    }

    @Override
    public boolean occursOn(int year, int month, int day){
        int timeStamp = year * 372 + month * 31 + day;
        return super.getTimeStamp() <= timeStamp;


    }

}
