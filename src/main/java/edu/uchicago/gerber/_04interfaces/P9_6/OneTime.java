package edu.uchicago.gerber._04interfaces.P9_6;

import java.util.Date;

public class OneTime extends Appointment{
    public OneTime(String strAppointment, int year, int month, int day) {
        super(strAppointment, year, month, day);
    }
}
