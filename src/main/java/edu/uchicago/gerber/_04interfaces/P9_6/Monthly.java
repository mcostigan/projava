package edu.uchicago.gerber._04interfaces.P9_6;

import java.util.Date;

public class Monthly extends Appointment{
    public Monthly(String strAppointment, int year, int month, int day) {
        super(strAppointment, year, month, day);
    }
    public boolean occursOn(int year, int month, int day) {
        int timeStamp = year*372+month*31+day;
        return (super.getTimeStamp()<=timeStamp && super.getDay()==day);


    }
}
