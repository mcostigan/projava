
package edu.uchicago.gerber._04interfaces.E9_11;

public class Person {
    private int yearOfBirth;
    private String name;

    public Person(int yearOfBirth, String name) {
        this.yearOfBirth = yearOfBirth;
        this.name = name;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "yearOfBirth=" + yearOfBirth +
                ", name='" + name + '\'' +
                '}';
    }
}
