package edu.uchicago.gerber._04interfaces.E9_11;

public class Instructor extends Person {
    private int salary;

    public Instructor(int yearOfBirth, String name, int salary) {
        super(yearOfBirth, name);
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Instructor{" + super.toString() + "," +
                "salary=" + salary +
                '}';
    }
}
