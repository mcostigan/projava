package edu.uchicago.gerber._04interfaces.E9_11;

public class Student extends Person{
    private String major;

    public Student(int yearOfBirth, String name, String major) {
        super(yearOfBirth, name);
        this.major = major;
    }

    public String getMajor() {
        return major;
    }

    public void changeMajor(String major) {
        this.major = major;
    }

    @Override
    public String toString() {
        return "Student{" + super.toString() + "," +
                "major='" + major + '\'' +
                '}';
    }
}
