package edu.uchicago.gerber._04interfaces.E9_11;

import java.util.ArrayList;

public class PersonDriver {
    public static void main(String[] args) {
        ArrayList<Person> people = new ArrayList<Person>();

        Instructor myInstructor = new Instructor(1980,"Adam Gerber",1_000_000);
        Student me = new Student(1994,"Matt Costigan","CS");
        System.out.println(me);
        System.out.println(myInstructor);
    }
}
