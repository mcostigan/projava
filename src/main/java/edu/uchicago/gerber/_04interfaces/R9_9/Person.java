package edu.uchicago.gerber._04interfaces.R9_9;

//import jdk.internal.net.http.frame.PriorityFrame;

public class Person {
}
class Employee extends Person{}
class Professor extends Employee{}
class TeachingAssistant extends Employee{}
class Secretary extends Employee{}
class DepartmentChair extends Professor{}
class Janitor extends Employee{}
class SeminarSpeaker extends Professor {}
class Course{}
class Seminar extends Course{}
class Lecture extends Course{}
class ComputerLab extends Course{}
