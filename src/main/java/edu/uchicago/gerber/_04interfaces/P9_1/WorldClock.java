package edu.uchicago.gerber._04interfaces.P9_1;

public class WorldClock extends Clock{
    private int offset;

    public WorldClock(int offset) {
        super();
        this.offset=offset;
    }
    @Override
    public int getHours(){
        return (super.getHours()+offset)%24;
    }
    @Override
    public int getMinutes(){
        return super.getMinutes();
    }
}
