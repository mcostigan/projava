package edu.uchicago.gerber._04interfaces.P9_1;

public class Clock {
    public int getHours(){
        return java.time.LocalTime.now().getHour();
    }

    public int getMinutes(){
        return java.time.LocalTime.now().getMinute();
    }

    public String getTime(){
        return getHours()+":"+getMinutes();
    }
}
