package edu.uchicago.gerber._04interfaces.P9_1;

public class ClockDriver {
    public static void main(String[] args) {
        Clock myClock = new Clock();
        Clock myWorldClock = new WorldClock(3);

        System.out.println(myClock.getHours()+":"+myClock.getMinutes());
        System.out.println(myWorldClock.getHours()+":"+myWorldClock.getMinutes());

        System.out.println(myClock.getTime());
        System.out.println(myWorldClock.getTime());
    }
}
