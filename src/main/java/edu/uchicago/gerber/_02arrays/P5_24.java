package edu.uchicago.gerber._02arrays;

import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Name your source files according to its Programming Exercise identifier, except replace the period with underscore.
 * For example, if the Programming Exercise identifier is P1.15, then name your java source file P1_15
 */
public class P5_24 {

    public static void main(String[] args) {
        // Dummy data for method
        System.out.println(numeralToDecimal("MCMLXXVIII"));

    }

    /**
     * Converts a roman numeral expression to a decimal
     *
     * @param roman a string of roman numerals
     * @return the decimal expression of roman
     */
    private static int numeralToDecimal(String roman) {
        //total = 0
        int total = 0;

        //Initialize values used in loop
        int firstValue;
        int secondValue;

        //While str is not empty
        while (roman.length() > 0) {
            //Get value of first character
            firstValue = letterToNumeric(roman.charAt(0));

            //If roman has more than one character...
            if (roman.length() > 1) {
                //get value of second character
                secondValue = letterToNumeric(roman.charAt(1));
            } else {
                //else assign 0
                secondValue = 0;
            }
            //If str has length 1, or value(first character of str) is at least value (second character of str)
            if (roman.length() == 1 || firstValue >= secondValue) {
                // Add value (first character of str)to total.
                total += firstValue;
                // Remove first character from str.
                roman = roman.substring(1);
            } else {
                // Add difference = value(second character of str) - value(first character of str)
                total += secondValue - firstValue;
                // Remove first character and second character from str.
                roman = roman.substring(2);
            }
        }
        return total;
    }

    /**
     * Looks up the value of a specific roman characters
     *
     * @param c character in roman numeral
     * @return the integer value of the character
     */
    private static int letterToNumeric(char c) {
        int result = 0;
        switch (c) {
            case 'I':
                result = 1;
                break;
            case 'V':
                result = 5;
                break;
            case 'X':
                result = 10;
                break;
            case 'L':
                result = 50;
                break;
            case 'C':
                result = 100;
                break;
            case 'D':
                result = 500;
                break;
            case 'M':
                result = 1000;
                break;
        }
        return result;
    }


}
