package edu.uchicago.gerber._02arrays;

import java.util.Scanner;

/**
 * Name your source files according to its Programming Exercise identifier, except replace the period with underscore.
 * For example, if the Programming Exercise identifier is P1.15, then name your java source file P1_15
 */
public class E6_1 {

    public static void main(String[] args) {
        // Initializes an array of 10 elements
        int[] arr = new int[10];

        //Assign each element of the array a random integer from 0-9, inclusive
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 10);
        }

        // Prints a line for all elements at an even index
        for (int i = 0; i < arr.length; i += 2) {
            System.out.print(arr[i] + " ");
        }
        // Return
        System.out.println();

        // Prints a line with every even element
        for (int v : arr) {
            if (v % 2 == 0) {
                System.out.print(v + " ");
            }
        }
        // Return
        System.out.println();

        // Print a line for all elements in reverse order
        for (int i = arr.length - 1; i > -1; i--) {
            System.out.print(arr[i] + " ");

        }
        // Return
        System.out.println();

        // Print first and last
        System.out.println(arr[0] + " " + arr[arr.length - 1]);
    }


}
