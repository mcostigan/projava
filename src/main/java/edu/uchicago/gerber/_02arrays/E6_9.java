package edu.uchicago.gerber._02arrays;

import java.util.Scanner;

/**
 * Name your source files according to its Programming Exercise identifier, except replace the period with underscore.
 * For example, if the Programming Exercise identifier is P1.15, then name your java source file P1_15
 */
public class E6_9 {

    public static void main(String[] args) {
        // Create dummy data for method
        int[] a = {1, 2, 3, 4};
        int[] b = {1, 2, 3, 5};
        System.out.println(equals(a, b));
    }

    /**
     * @param a an array of integers
     * @param b an array of integers
     * @return True if a and b have the same elements, else false
     */
    public static boolean equals(int[] a, int[] b) {
        // Cannot be equal if of different lengths
        if (a.length != b.length) {
            return false;
        }

        // If of same length, must have same integer value at each index
        for (int i = 0; i < a.length; i++) {
            // If not equal, return false
            if (a[i] != b[i]) {
                return false;
            }
        }

        // If equal for all i=1...a.length, then true
        return true;
    }


}
