package edu.uchicago.gerber._02arrays;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.Scanner;

public class CSVReader {
    private ArrayList<ArrayList<String>> arrCSV;

    /**
     * Construct CSVReader object from file path. Created nested ArrayLists, splitting values into rows and columns
     *
     * @param fileCSV File object of CSV to read
     */
    public CSVReader(File fileCSV) throws FileNotFoundException{
        // Initialize local scanner
        Scanner scanCSV = new Scanner(System.in);

        // Handle exceptions, from 0_1 sample code
        try {
            scanCSV = new Scanner(fileCSV);
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("Invalid file path :" + fileCSV.getAbsolutePath());
        }

        // Initialize return arraylist, which is a ragged arraylist of rows and columns
        arrCSV = new ArrayList<ArrayList<String>>();

        // Loop through lines in csv
        while (scanCSV.hasNextLine()) {
            // Create a new column array list for current row
            ArrayList<String> cols = new ArrayList<String>();

            // Store current row as string
            String strLine = scanCSV.nextLine();

            // Initialize in quotes flag
            // a comma within quotation marks is not a delimiter
            boolean blnInQuotes = false;

            // Value to store where in the string the current field starts
            // First field starts at position 0
            int iFieldStart = 0;

            // Loop through each character in string
            for (int i = 0; i < strLine.length(); i++) {

                // Get char i from strLine
                char thisChar = strLine.charAt(i);

                // If thisChar is a quote, toggle blnInQuote
                if (thisChar == '\"') {
                    // Toggle boolean indicator
                    blnInQuotes = !blnInQuotes;

                } else if (thisChar == ',' && !blnInQuotes) {
                    // If char is a comma and is not between quotes, increment fields
                    cols.add(strLine.substring(iFieldStart, i).trim());
                    iFieldStart = i + 1;
                }

            }
            // Add last field to cols, since it is not followed by a comma
            cols.add(strLine.substring(iFieldStart).trim());

            // Add a new item to rows containing all cols arraylist
            arrCSV.add(cols);
        }
        scanCSV.close();

    }

    /**
     *
     * @return number of rows in arrCSV as int
     */
    public int numberOfRows() {
        return arrCSV.size();
    }

    /**
     *
     * @param row index of row in arraylist
     * @return number of fields in row
     * @throws IllegalArgumentException if row is not 0<=row<=size-1
     */
    public int numberOfFields(int row) throws IllegalArgumentException {
        if (row<0 || row>arrCSV.size()-1){
            throw new IllegalArgumentException("Invalid row value");
        }
        return arrCSV.get(row).size();
    }

    /**
     *
     * @param row index of row of arraylist
     * @param col index of col of arraylist
     * @return string of value stored in [row,col]
     * @throws IllegalArgumentException if row, col out of scope
     */
    public String field(int row, int col) throws IllegalArgumentException{
        if (row<0 || row>arrCSV.size()-1){
            throw new IllegalArgumentException("Invalid row value");
        }
        if (col<0 || col>arrCSV.get(row).size()-1){
            throw new IllegalArgumentException("Invalid column value");
        }
        return arrCSV.get(row).get(col);
    }

}

