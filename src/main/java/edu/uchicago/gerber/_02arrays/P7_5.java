package edu.uchicago.gerber._02arrays;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Name your source files according to its Programming Exercise identifier, except replace the period with underscore.
 * For example, if the Programming Exercise identifier is P1.15, then name your java source file P1_15
 */
public class P7_5 {
    public static void main(String[] args) {
        // NOTE : bulk of code in CSV reader object
        // Initialize scanner
        Scanner scan = new Scanner(System.in);

        // Prompt for file path
        System.out.print("What is the absolute path to your input file?:");

        // Store file path
        String strPath = scan.next();

        // Open file
        File fileCSV = new File(strPath);
        scan.close();

        // Create csv object
        CSVReader arrCSV;

        // create CSV object and handle errors
        try {
            arrCSV = new CSVReader(fileCSV);
            // Call csv methods
            System.out.println(arrCSV.numberOfRows());
            System.out.println(arrCSV.numberOfFields(1));
            System.out.println(arrCSV.field(1, 1));
        } catch (FileNotFoundException | IllegalArgumentException exc){
            System.out.println(exc.getMessage());
        }
    }
}
