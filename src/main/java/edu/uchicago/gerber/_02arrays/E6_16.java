package edu.uchicago.gerber._02arrays;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Name your source files according to its Programming Exercise identifier, except replace the period with underscore.
 * For example, if the Programming Exercise identifier is P1.15, then name your java source file P1_15
 */
public class E6_16 {

    public static void main(String[] args) {

        // Get user inputs
        ArrayList<Integer> intInputs = getIntegerInput();

        // Print array as histogram
        printHistogram(intInputs);
    }

    /**
     * Prints an array of integers as a vertical histogram
     *
     * @param arr an array list of integers
     */
    public static void printHistogram(ArrayList<Integer> arr) {
        // Set max height to 20 stars
        final int MAX_HEIGHT = 20;

        // Set max equal to first value in array
        int max = arr.get(0);

        // Iterate through second to  last elements and find the series max
        for (int i = 1; i < arr.size(); i++) {
            max = Math.max(max, arr.get(i));
        }

        // For each i=MAX_HEIGHT, MAX_HEIGHT-1,...,1 cutOff represents the i/MAX_HEIGHT of the max value
        // So, for an element in arr to get a star in row i, the row's value must be >= max*i/MAX_HEIGHT

        // Loop through rows MAX_HEIGHT to 1
        for (int i = MAX_HEIGHT; i > 0; i--) {
            // Calculate the minimum value necessary for each element in arr to get a star
            double cutOff = (max * i) / (double) MAX_HEIGHT;

            // Loop through each element in arr
            for (int j : arr) {
                // Give a star if element is >= to cutOff
                if (j >= cutOff) {
                    System.out.print('*');
                } else {
                    //Else, empty char
                    System.out.print(' ');
                }
            }
            // Move to next line
            System.out.println();
        }
    }

    /**
     * Prompts the user for an input of integers
     *
     * @return an array list of inputted integers
     * Source : much of code from sample code provided in P0_0 of _01control
     */
    public static ArrayList<Integer> getIntegerInput() throws IllegalArgumentException{
        Scanner scan = new Scanner(System.in);

        // Paraphrased from sample code in P0_0
        ArrayList<Integer> intInputs = new ArrayList<>();

        //Prompt for first integer
        System.out.println("Please enter an integer ");

        //Prompt until first integer is valid
        while (!scan.hasNextInt()) {
            System.out.println("Oops! Please enter an valid integer:");
            scan.next();
        }

        //Store value
        intInputs.add(scan.nextInt());

        // Get more integers from user
        while (true) {
            try {
                System.out.print("Type another integer value or \"quit\" to exit:");
                //any value that is not an integer (including "quit") will throw an exception, which breaks out of the loop
                intInputs.add(scan.nextInt());
            } catch (Exception e) {
            // Move on if a non-int is entered
                break;
            }
        }

        scan.close();
        return intInputs;
    }
}



