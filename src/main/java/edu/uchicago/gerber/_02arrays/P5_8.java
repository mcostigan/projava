package edu.uchicago.gerber._02arrays;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Name your source files according to its Programming Exercise identifier, except replace the period with underscore.
 * For example, if the Programming Exercise identifier is P1.15, then name your java source file P1_15
 */
public class P5_8 {

    public static void main(String[] args) {
        // Dummy data for method
        System.out.println(isLeapYear(2009));
    }

    /**
     * Determines if a specific year is a leap year
     *
     * @param year year to check
     * @return true if year is leap year, else false
     */
    private static boolean isLeapYear(int year) {
        // If divisible by 400, then leap year
        if (year % 400 == 0) {
            return true;
        }
        // If divisible by 4 and not 100, then leap year
        else return year % 4 == 0 && year % 100 != 0;
    }


}
