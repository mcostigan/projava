package edu.uchicago.gerber._02arrays;

import java.util.Arrays;

/**
 * Name your source files according to its Programming Exercise identifier, except replace the period with underscore.
 * For example, if the Programming Exercise identifier is P1.15, then name your java source file P1_15
 */
public class E6_12 {

    public static void main(String[] args) {
        // Initializes an array
        int[] arr = new int[20];

        //Assign each element of the array a random integer from 0-99, inclusive
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100);
        }

        // Print unsorted array
        printArray(arr);

        // Sort with standard library
        Arrays.sort(arr);

        // Print sorted array
        printArray(arr);
    }

    /**
     * Prints an array of integers with values seperated by spaces
     *
     * @param a an array of integers
     */
    public static void printArray(int[] a) {
        // For each element in the array
        for (int i : a) {
            // Print element and blank space on current line
            System.out.print(i + " ");
        }
        // Return
        System.out.println();

    }
}



