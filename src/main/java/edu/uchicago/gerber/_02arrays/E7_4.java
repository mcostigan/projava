package edu.uchicago.gerber._02arrays;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Name your source files according to its Programming Exercise identifier, except replace the period with underscore.
 * For example, if the Programming Exercise identifier is P1.15, then name your java source file P1_15
 */
public class E7_4 {

    public static void main(String[] args) throws FileNotFoundException {
        // Initialize scanner
        Scanner scan = new Scanner(System.in);

        // Prompt for file path
        System.out.print("What is the absolute path to your input file?:");

        // Store file path
        String strPath = scan.next();

        // Open input file
        File fileInput = new File(strPath);

        // Handle exceptions, from 0_1 sample code
        try {
            scan = new Scanner(fileInput);
        } catch (FileNotFoundException e) {
            System.out.println("Invalid file path");
            return;
        }

        // Prompt for output file path
        Scanner in = new Scanner(System.in);
        System.out.print("What is the absolute path to your output file?:");

        // Store file path
        strPath = in.next();

        // Open/create output file
        // wrap in try so printwriter is closed at the end of the try
        try (PrintWriter out = new PrintWriter(strPath)) {
            // Start counter
            int nC = 1;

            // Initialize line number string
            String strLineNumber;

            // Loop through lines in file
            while (scan.hasNextLine()) {
                // Compose line number string
                // Number takes of two spaces, and a leading and trailing space
                strLineNumber = "/* " + String.format("%2s", nC) + " */ ";

                // Print line number then line
                out.print(strLineNumber);
                out.println(scan.nextLine());

                // Increment line number
                nC++;
            }
        }
        scan.close();
        in.close();
    }
}
