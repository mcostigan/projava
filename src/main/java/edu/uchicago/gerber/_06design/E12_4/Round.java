package edu.uchicago.gerber._06design.E12_4;

import java.util.ArrayList;
import java.util.Scanner;

public class Round {
    private int score;
    private int level;
    private ArrayList<Question> roundQuestions;

    public Round(int level) {
        this.level = level;
        this.score = 0;
        this.roundQuestions = new ArrayList<Question>();
        roundQuestions.add(new Question(level));

    }

    public int getScore() {
        return score;
    }

    public String getQuestion() {
        return roundQuestions.get(roundQuestions.size() - 1).toString();
    }

    public void answerQuestion() {
        Scanner in = new Scanner(System.in);
        System.out.println("Answer: ");
        Question currentQuestion = roundQuestions.get(roundQuestions.size() - 1);
        int response = in.nextInt();
        if (currentQuestion.answer(response)) {
            System.out.println("Correct!");
            score += 1;
        } else {
            System.out.println("Oops! Try again: ");
            response = in.nextInt();
            if (currentQuestion.answer(response)) {
                score += 1;
                System.out.println("Correct");
            } else {
                System.out.println("Oops! That's not right. Let's try another one...");
            }
        }
        if (score < 5) {
            roundQuestions.add(new Question(level));
        } else {
            System.out.println("Great job! You've completed level " + level);
        }
    }

}
