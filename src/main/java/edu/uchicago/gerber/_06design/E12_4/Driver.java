package edu.uchicago.gerber._06design.E12_4;

import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        Round currentRound = new Round(1);
        while (currentRound.getScore()<5){
            System.out.println(currentRound.getQuestion());
            currentRound.answerQuestion();
        }

        currentRound = new Round(2);
        while (currentRound.getScore()<5){
            System.out.println(currentRound.getQuestion());
            currentRound.answerQuestion();
        }

        currentRound = new Round(3);
        while (currentRound.getScore()<5){
            System.out.println(currentRound.getQuestion());
            currentRound.answerQuestion();
        }

    }
}
