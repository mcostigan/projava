package edu.uchicago.gerber._06design.E12_4;


import java.util.Random;

public class Question {
    private int x;
    private int y;
    private int operation;
    private int level;

    public Question(int level) {
        this.level = level;
        Random rand = new Random();

        switch (level) {
            case 1:
                x = 1 + rand.nextInt(7);
                y = 1 + rand.nextInt(8 - x);
                operation = 1;
                break;
            case 2:
                x = 1 + rand.nextInt(8);
                y = 1+ rand.nextInt(8);
                operation=1;
                break;
            case 3:
                x = 1+rand.nextInt(8);
                y = rand.nextInt(x);
                operation=-1;
                break;
        }
    }

    public boolean answer(int ans) {
        return ans == x + (operation * y);
    }

    @Override
    public String toString() {
        char arithmetic_operation;
        if (operation == 1) {
            arithmetic_operation = '+';
        } else {
            arithmetic_operation = '-';
        }
        return "Question: " + x +
                arithmetic_operation + y+"?";
    }
}
