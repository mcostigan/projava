package edu.uchicago.gerber._06design.P12_1;

public class Item {
    private String itemDescription;
    private double itemPrice;
    private int itemQuantity;

    public Item(String itemDescription, double itemPrice, int itemQuantity) {
        this.itemDescription = itemDescription;
        this.itemPrice = itemPrice;
        this.itemQuantity = itemQuantity;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(int itemQuantity) {
        this.itemQuantity = itemQuantity;
    }
    public void decrement(){
        this.itemQuantity--;
    }

    @Override
    public String toString() {
        return itemDescription + ", Price: " + String.format("%.2f",itemPrice) + ", Quantity: " + itemQuantity;
    }
}
