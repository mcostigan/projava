package edu.uchicago.gerber._06design.P12_1;

import java.util.ArrayList;
import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int mySelection;

        // Prepopulate vending machine
        VendingMachine myVM = new VendingMachine();
        myVM.addItem("Doritos", 1.5, 10);
        myVM.addItem("Lays", 1.5, 10);
        myVM.addItem("Lifesavers", .75, 50);
        myVM.addItem("Skittles", 1, 25);
        myVM.addItem("Jonathan's favorite candy", .01, 100);

        // Present user with options
        System.out.println("[0]: Buy an item");
        System.out.println("[1]: Add an item");
        System.out.println("[2]: Restock an item");
        System.out.println("[3]: Cash out");
        System.out.println("[4]: End");

        try {
            // Proceed with corresponding action
            int action = getIntegerInput(in, 0, 4, "Select an action:");
            while (true) {
                if (action == 4) {
                    break;
                }
                switch (action) {
                    // Buy an item
                    case 0:
                        // Print possible options
                        printItems(myVM);

                        // Ask user for input
                        mySelection = getIntegerInput(in, 0, myVM.getItems().size() - 1, "Choose an item:");

                        // Receive payment
                        enterPayment(myVM);

                        // Attempt transaction
                        System.out.println(myVM.buyItem(mySelection));
                        break;

                    // Add an item
                    case 1:
                        // Prompt operator for new item details
                        System.out.println("Enter item name: ");
                        String name = in.next();
                        int quantity = getIntegerInput(in, 0, 1000, "Enter item quantity: ");
                        System.out.println("Enter item price: ");
                        double price = in.nextDouble();

                        // Add item to vm and print result
                        System.out.println(myVM.addItem(name, price, quantity));
                        break;

                    // Restock an item
                    case 2:
                        // Prompt user for item to restock and new quantity
                        printItems(myVM);
                        int addItem = getIntegerInput(in, 0, myVM.getItems().size() - 1, "Select an item to restock: ");
                        int addQuantity = getIntegerInput(in, 0, 1000, "What is the new quantity: ");

                        // restock and print result
                        System.out.println(myVM.restockItem(addItem, addQuantity));
                        break;

                    case 3:
                        System.out.println(myVM.cashOut());


                }

                // Present user with options
                System.out.println("[0]: Buy an item");
                System.out.println("[1]: Add an item");
                System.out.println("[2]: Restock an item");
                System.out.println("[3]: Cash out");
                System.out.println("[4]: End");
                action = getIntegerInput(in, 0, 4, "Select an action:");
            }


        } catch (Exception e) {
            System.out.println("Transaction cancelled. Invalid input. " + e.getMessage());
        }
    }

    /**
     * Allows a user to enter coins into a vending machine
     *
     * @param myVM current vending machine object
     */
    public static void enterPayment(VendingMachine myVM) {
        Scanner in = new Scanner(System.in);
        // Create acceptable coins
        Coin dollar = new Coin(1.00, "dollar");
        Coin quarter = new Coin(.25, "quarter");
        Coin dime = new Coin(.10, "dime");
        Coin nickel = new Coin(.05, "nickel");
        Coin penny = new Coin(.01, "penny");
        Coin[] coins = {penny, nickel, dime, quarter, dollar};

        // List coin options
        for (int i = 0; i < coins.length; i++) {
            System.out.println("[" + i + "]: " + coins[i].toString());
        }
        System.out.println("[" + coins.length + "]: Finished");

        // Prompt user to enter coins
        int userSelection = getIntegerInput(in, 0, coins.length, "Add a coin: ");
        while (true) {
            if (userSelection == coins.length) {
                break;
            } else {
                System.out.println(myVM.addCoin(coins[userSelection]));
            }
            userSelection = getIntegerInput(in, 0, coins.length, "Add a coin: ");
        }
    }

    /**
     * prints all items in a vending machine
     *
     * @param myVM current vending machine object
     */
    public static void printItems(VendingMachine myVM) {
        // Print possible options
        ArrayList<Item> items = myVM.getItems();
        for (int i = 0; i < items.size(); i++) {
            Item currentItem = items.get(i);
            System.out.println("[" + i + "] : " + currentItem.toString());
        }
    }

    public static int getIntegerInput(Scanner in, int low, int high, String prompt) throws IllegalArgumentException {
            try {
                System.out.print(prompt);
                int userInput = in.nextInt();
                if (userInput < low || userInput > high) {
                    throw new IllegalArgumentException("Input out of bounds");
                } else {
                    return userInput;
                }
            } catch (Exception e) {
                throw new IllegalArgumentException("Invalid input." + e.getMessage());
            }
    }

}
