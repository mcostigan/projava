package edu.uchicago.gerber._06design.P12_1;

import java.util.ArrayList;

public class VendingMachine {
    // Vending machine contains an array list of items...
    private ArrayList<Item> items;

    // ... money it has stored from previous transactions
    private double money;

    // ... and money from the current transaction, which may be returned
    private double currentPayment;

    /**
     * Construct vending machine
     * initially has no money and no items
     */
    public VendingMachine() {
        money = 0;
        this.items = new ArrayList<Item>();
        this.currentPayment= 0;
    }

    /**
     * Prints out all items with a quantity > 0
     */
    public ArrayList<Item> getItems(){
        return items;
    }

    /**
     * Adds a new item to the items array list from given inputs
     * @param itemDescription Name/description of item to add
     * @param itemPrice price of item
     * @param itemQuantity initial quantity of item
     */
    public String addItem(String itemDescription, double itemPrice, int itemQuantity){
        items.add(new Item(itemDescription,itemPrice,itemQuantity));
        return items.get(items.size()-1).toString();
    }

    /**
     * Restocks a particular item to a given quantity
     * @param itemID item position in items array list
     * @param newQuantity the updated quantity of the item
     */
    public String restockItem(int itemID, int newQuantity){
        items.get(itemID).setItemQuantity(newQuantity);
        return items.get(itemID).toString();
    }

    /**
     * Removes all cash from the machine
     */
    public String cashOut(){
        double tmpMoney = money;
        money=0;
        return "Money removed: " + String.format("%.2f",tmpMoney);
    }

    /**
     * Attempts to buy a specified item with the currently inserted coins
     * @param itemID the item position in the items arraylist
     */
    public String buyItem(int itemID){
        Item currentItem = items.get(itemID);
        if (currentItem.getItemQuantity()==0){
            returnFunds();
            return "We're out of " + currentItem.getItemDescription() + "\nCoins returned...";
        }
        else if (currentPayment>=currentItem.getItemPrice()){
            currentItem.decrement();
            money += currentPayment;
            currentPayment=0;
            return "Success! Dispensing " + currentItem.getItemDescription();
        }
        else {
            return "Insufficient funds.\n" + returnFunds();
        }

    }

    /**
     * Accepts user payment
     * @param coin the coin inserted for payment
     */
    public String addCoin(Coin coin){
        currentPayment+=coin.getValue();
        return coin.getName() + " added";
    }

    /**
     * returns the current payment to the user
     */
    public String returnFunds(){
        double tmpMoney = currentPayment;
        currentPayment=0;
        return "Money returned: " + String.format("%.2f",tmpMoney);
    }

}
