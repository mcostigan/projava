package edu.uchicago.gerber._06design.P12_1;

public class Coin {
    private double value;
    private String name;

    public Coin(double value, String name) {
        this.value = value;
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name +" (" + String.format("%.2f",value) +")";
    }
}
