package edu.uchicago.gerber._06design.R12_10;
import java.util.ArrayList;
import java.util.Scanner;

class Quiz

{
    private ArrayList<Question> questions;

    /**
     Constructs a quiz with no questions.
     */
    public Quiz()
    {
        questions = new ArrayList<Question>();
    }

    /**
     Adds a question to this quiz.
     @param q the question
     */
    public void addQuestion(Question q)
    {
        questions.add(q);
    }

    /**
     Presents the questions to the user and checks the response.
     */
    public void presentQuestions()
    {
        Scanner in = new Scanner(System.in);

        for (Question q : questions)
        {
            q.display();
            System.out.print("Your answer: ");
            String response = in.nextLine();
            System.out.println(q.checkAnswer(response));
        }
    }
}

class ChoiceQuestion extends Question
{
    private ArrayList<String> choices;

    /**
     Constructs a choice question with no choices.
     */
    public ChoiceQuestion()
    {
        choices = new ArrayList<String>();
    }

    /**
     Adds an answer choice to this question.
     @param choice the choice to add
     @param correct true if this is the correct choice, false otherwise
     */
    public void addChoice(String choice, boolean correct)
    {
        choices.add(choice);
        if (correct)
        {
            // Convert choices.size() to string
            String choiceString = "" + choices.size();
            setAnswer(choiceString);
        }
    }

    public void display()
    {
        // Display the question text
        super.display();
        // Display the answer choices
        for (int i = 0; i < choices.size(); i++)
        {
            int choiceNumber = i + 1;
            System.out.println(choiceNumber + ": " + choices.get(i));
        }
    }
}
class Question
{
    private String text;
    private String answer;

    /**
     Constructs a question with empty question and answer.
     */
    public Question()
    {
        text = "";
        answer = "";
    }

    /**
     Sets the question text.
     @param questionText the text of this question
     */
    public void setText(String questionText)
    {
        text = questionText;
    }

    /**
     Sets the answer for this question.
     @param correctResponse the answer
     */
    public void setAnswer(String correctResponse)
    {
        answer = correctResponse;
    }

    /**
     Checks a given response for correctness.
     @param response the response to check
     @return true if the response was correct, false otherwise
     */
    public boolean checkAnswer(String response)
    {
        return response.equals(answer);
    }

    /**
     Displays this question.
     */
    public void display()
    {
        System.out.println(text);
    }
}