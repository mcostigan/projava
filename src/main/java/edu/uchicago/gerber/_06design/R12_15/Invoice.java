package edu.uchicago.gerber._06design.R12_15;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Invoice {
    Customer invoiceCustomer;
    double invoiceTotal;
    double amountDue;

    public void calculateTotal(){

    }

    public void addProduct(){

    }

    public void addPayment(){

    }



}
class Customer {
    String name;
    Address billingAddress;
    Address shippingAddress;
    ArrayList<Product> purchaseHistory;
    ArrayList<Payment> paymentHistory;

}

class Address {
    String street;
    String city;
    String state;
    String zip;

}

class Product{
    String description;
    Double price;

}

class Payment {
    double amount;

}